+ function($) {

    $(".color-box-gr").colorbox({ rel: 'group1', maxWidth: "95%", maxHeight: "95%" });
    $("#basicExample").justifiedGallery({
        rowHeight: 160,
        lastRow: 'nojustify',
        margins: 8
    });


}(jQuery);



$(window).resize(function() {
    $('.flexslider.top .slides img').css('margin-left', 0);
    $('.flexslider.top .slides img').css('margin-left', (($(window).outerWidth() - $('.flexslider .slides img').outerWidth()) / 2) + 'px');
    $('.text-slide').css('margin-top', 0);
    $('.text-slide').css('margin-top', (($('ul.slides').outerHeight() - $('.text-slide').outerHeight()) / 2) + 'px');

});

$(window).on('load', function(event) {
    $('.flexslider.top').flexslider({
        animation: "fade",
        controlNav: false,
    });

    $('.flexslider.news-front').flexslider({
        animation: "slide",
        animationLoop: true,
        slideshow: true,
        itemWidth: 220,
        itemMargin: 0,
        minItems: 0,
        maxItems: 3,
        move: 1,
        controlNav: false,
    });

    $('.grid').masonry({
        // use a separate class for itemSelector, other than .col-
        itemSelector: '.grid-item',
        columnWidth: '.grid-item',
        percentPosition: true
    });



    $('.flexslider.top .slides img').css('margin-left', (($(window).outerWidth() - $('.flexslider .slides img').outerWidth()) / 2) + 'px');
    $('.text-slide').css('margin-top', (($('ul.slides').outerHeight() - $('.text-slide').outerHeight()) / 2) + 'px');
});