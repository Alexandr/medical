+ function($) {


    $('.map-adr').each(function(index, el) {
        var mapText = $(this).find('.map-text').text();
        var idMap = $(this).attr('id');
        var mapTextS = mapText.toString();
        var arrt = mapText.replace(',', ' ');
        var arr = arrt.split(' ');
        var coordOne = arr[0];
        var coordTwo = arr[1];

        ymaps.ready(init);
        var myMap,
            myPlacemark;

        function init() {
            myMap = new ymaps.Map(idMap, {
                center: [coordOne, coordTwo],
                zoom: 15
            });

            myPlacemark = new ymaps.Placemark([coordOne, coordTwo], {

            });

            myMap.geoObjects.add(myPlacemark);
        }

    });

    $('.item-adr').each(function(index, el) {
        $(this).click(function(event) {
            var data = $(this).attr('data');
            if ($('.map-wr' + data).hasClass('h-300')) {
                $('.map-wr' + data).removeClass('h-300');
            } else {
                $('.map-wrap').removeClass('h-300');
                $('.map-wr' + data).addClass('h-300');
            }

        });
    });




}(jQuery);