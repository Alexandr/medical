+ function($) {

    $('.text-full').each(function(index, el) {
        var textName = $(this).attr('name');
        CKEDITOR.replace(textName, {
            filebrowserBrowseUrl: '/web/lib/elfinder/elfinder.html', // eg. 'includes/elFinder/elfinder-cke.html'
        });
    });

    $(".dynamicform_wrapper").on("afterInsert", function(e, item) {
        $('textarea.text-full').closest('.form-group').each(function(index, el) {
            if (!$(this).find('div').hasClass('cke')) {
                var textName1 = $(this).find('textarea.text-full').attr('name');
                CKEDITOR.replace(textName1, {
                    filebrowserBrowseUrl: '/web/lib/elfinder/elfinder.html', // eg. 'includes/elFinder/elfinder-cke.html'
                });
            }
        });
    });

}(jQuery);