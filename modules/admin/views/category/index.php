<?php

use yii\helpers\Html;
use yii\grid\GridView;
use leandrogehlen\treegrid\TreeGrid;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Категории';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить категорию', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?=
    TreeGrid::widget([
        'dataProvider' => $dataProvider,
        'keyColumnName' => 'id',
        'parentColumnName' => 'parent_id',
        'parentRootValue' => '0', //first parentId value
        'pluginOptions' => [
            'initialState' => 'expanded',
        ],
        'columns' => [
            'id',
            'name',
            /* [
              'attribute' => 'parent_id',
              'value' => function ($data) {
              return $data->parent_id == 0 ? 'Основной' : \app\models\Category::findOne(['id' => $data->parent_id])->name;
              }
              ], */
            'weight',
            'alias',
            [
                'value' => function ($data) {
                    return Html::a('<i class="fa fa-pencil-square-o" aria-hidden="true"></i>', ['update', 'id' => $data->id], [
                                'class' => 'btn btn-success btn-sm',
                                'data' => [
                                    'toggle' => 'tooltip',
                                ],
                                'title' => 'Изменить',
                            ]) . ' ' . Html::a('<i class="fa fa-eye" aria-hidden="true"></i>', ['view', 'id' => $data->id], [
                                'class' => 'btn btn-default btn-sm',
                                'data' => [
                                    'toggle' => 'tooltip',
                                ],
                                'title' => 'Просмотр',
                            ]) . ' ' . Html::a('<i class="fa fa-trash-o" aria-hidden="true"></i>', ['delete', 'id' => $data->id], [
                                'class' => 'btn btn-danger btn-sm',
                                'data' => [
                                    'confirm' => 'Are you sure you want to delete this item?',
                                    'method' => 'post',
                                    'toggle' => 'tooltip',
                                ],
                                'title' => 'Удалить',
                    ]);
                },
                'format' => 'raw',
            ],
        //['class' => 'yii\grid\ActionColumn']
        ]
    ]);
    ?>
</div>
