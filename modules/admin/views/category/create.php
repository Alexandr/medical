<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Category;


/* @var $this yii\web\View */
/* @var $model app\models\Category */

$this->title = 'Добавить категорию';
$this->params['breadcrumbs'][] = ['label' => 'Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$mapCategoy = new Category();
$parent = ArrayHelper::merge(['0' => 'Основная'], $mapCategoy->getTree());
?>
<div class="category-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'parent' => $parent,
        'model' => $model,
    ]) ?>

</div>
