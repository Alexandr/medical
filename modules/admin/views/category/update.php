<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Category;

/* @var $this yii\web\View */
/* @var $model app\models\Category */

$this->title = 'Обновить категорию: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновить';
$mapCategoy = new Category();
$key = $mapCategoy->getTree();
unset($key[$model->id]);
$parent = ArrayHelper::merge(['0' => 'Основная'], $key);
?>
<div class="category-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'parent' => $parent,
        'model' => $model,
    ]) ?>

</div>
