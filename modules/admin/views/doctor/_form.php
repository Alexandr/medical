<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Services;
use app\models\CategoryServices;

/* @var $this yii\web\View */
/* @var $model app\models\Doctor */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsFile(
        '@web/web/lib/ckeditor/ckeditor.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);

$this->registerJsFile(
        '@web/js/ckeditor.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);

$this->registerJsFile(
        '@web/js/translitCyr.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);
$modelServise = new CategoryServices();
$dataArray=[];
$modelServiseArray = $modelServise->getTree();

foreach ($modelServiseArray as $key => $valServCat) {    
    $name = strval($modelServiseArray[$key]);
    $dataArray = ArrayHelper::merge($dataArray, array($name => $name));
    $dataArraySev = ArrayHelper::map(Services::find()->where(['category_id' => $key])->all(), 'id', 'name');
    $dataArray[$name] = $dataArraySev;
}

?>

<div class="doctor-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true,
        'class' => 'input-in form-control'])
    ?>

    <?php if (!$model->isNewRecord): ?>
        <div class="img-pic"><img src="/web/<?= $image->getPath('x200') ?>" alt=""></div>
    <?php endif; ?>

    <?= $form->field($model, 'imageFile')->fileInput() ?>

    <?= $form->field($model, 'body')->textarea(['class' => 'form-control text-full']) ?>

    <?=
    $form->field($model, 'services_array')->widget(Select2::classname(), [
        'data' => $dataArray,
        'language' => 'ru',
        'options' => [
            'placeholder' => 'Select a state ...',
            'multiple' => true,
        ],
        'pluginOptions' => [
            'tags' => true,
            'tokenSeparators' => [',', ' '],
            'maximumInputLength' => 10
        ],
    ])
    ?>


    <?= $form->field($model, 'date_create')->textInput() ?>

    <?= $form->field($model, 'count')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'alias')->textInput(['maxlength' => true,
        'class' => 'input-out form-control'])
    ?>

    <?= $form->field($model, 'weight')->textInput() ?>

        <?= $form->field($model, 'user_id')->hiddenInput()->label(false) ?>

    <div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>

</div>
