<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Doctor */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Doctors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$image = $model->getImage();
?>
<div class="doctor-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ])
        ?>
    </p>
    
    <h3>Услуги</h3>
    <div class="row">
        <?php foreach ($model->services as $service): ?>
        <h4><?= $service->name ?></h4>
        <?php endforeach; ?>
    </div>



    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'Foto',
                'value' => '<img src="/'.$image->getPath('x200').'" alt="">',
                'format' => 'raw',
            ],
            'name',
            'body:raw',
            'date_create:date',
            'count',
            'alias',
            'weight',
            'user.username',
        ],
    ])
    ?>
    

</div>
