<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FrontContent */

$this->title = 'Обновить содержимое на главной';
$this->params['breadcrumbs'][] = ['label' => 'Админ', 'url' => ['/admin']];
$this->params['breadcrumbs'][] = ['label' => 'Содержимое на главной', 'url' => ['view']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="front-content-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
