<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Category;
use kartik\datecontrol\DateControl;

/* @var $this yii\web\View */
/* @var $model app\models\Article */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsFile(
        '@web/web/lib/ckeditor/ckeditor.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);

$this->registerJsFile(
        '@web/js/ckeditor.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);
$this->registerJsFile(
        '@web/js/translitCyr.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);
$categoryMap = new Category();
?>


<div class="article-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true,
        'class' => 'input-in form-control']) ?>

    <?= $form->field($model, 'category_id')->dropDownList(ArrayHelper::merge(['0' => '-- Выбирете категорию --'], $categoryMap->getTree())) ?>

    <?= $form->field($model, 'body')->textarea(['class' => 'form-control text-full']) ?>

    <?= $form->field($model, 'frontpage')->checkbox() ?>

    <?= $form->field($model, 'count')->hiddenInput()->label(false) ?>

    <?=
    $form->field($model, 'date_create')->widget(DateControl::classname(), [
        'displayFormat' => 'php:d-m-Y',
        'type' => DateControl::FORMAT_DATE
    ])
    ?>

    <?= $form->field($model, 'alias')->textInput(['maxlength' => true,
        'class' => 'input-out form-control']) ?>

<?= $form->field($model, 'user_id')->hiddenInput()->label(false) ?>

    <div class="form-group">
<?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>

</div>
