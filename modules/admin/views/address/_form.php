<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Address */
/* @var $form yii\widgets\ActiveForm */
//$this->registerJsFile(
//        'https://api-maps.yandex.ru/2.1/?lang=ru_RU'
//);

$this->registerJsFile(
        '@web/web/js/event_reverse_geocode.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);

$this->registerCss('#map{ width: 100%; height: 300px; float: left;}');

?>

<div class="address-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'telephon')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'map')->hiddenInput(['class' => 'map-input form-control'])->label(FALSE) ?>
    <div id="map"></div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
