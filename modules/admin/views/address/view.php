<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Address */

$this->title = $model->address;
$this->params['breadcrumbs'][] = ['label' => 'Addresses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile(
    '@web/web/js/map.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);

$this->registerCss('#map{ width: 300px; height: 300px; float: left;}');

?>
<div class="address-view">
    

    <h1><?= Html::encode($this->title) ?></h1>
    
    <div class="hide map-text"><?= $model->map ?></div>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'telephon',
            'address',
            [
                'attribute' => 'map',
                'value' => '<div id="map"></div>',
                'format' => 'raw',
            ],
        ],
    ]) ?>

</div>
