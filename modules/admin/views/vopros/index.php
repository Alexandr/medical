<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\VoprosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Вопросы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vopros-index">

    <h1><?= Html::encode($this->title) ?></h1>


    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'doctor_id',
            'name',
            'telephone',
            'date_create:date',
            'email:email',
            'vopros:ntext',
            [
                'value' => function ($data) {
                    return Html::a('<i class="fa fa-pencil-square-o" aria-hidden="true"></i> Ответить', ['update', 'id' => $data->id], [
                                'class' => 'btn btn-success btn-sm',
                                'data' => [
                                    'toggle' => 'tooltip',
                                ],
                                'title' => 'Ответить',
                            ]) . ' ' . Html::a('<i class="fa fa-trash-o" aria-hidden="true"></i> Удалить', ['delete', 'id' => $data->id], [
                                'class' => 'btn btn-danger btn-sm',
                                'data' => [
                                    'confirm' => 'Are you sure you want to delete this item?',
                                    'method' => 'post',
                                    'toggle' => 'tooltip',
                                ],
                                'title' => 'Удалить',
                    ]);
                },
                'format' => 'raw',
            ],
        // 'otvet:ntext',
        //['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>
</div>
