<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Vopros */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vopros-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    if ($model->doctor_id) {
        echo 'Врач: <b>'.$model->doctor->name.'</b>';
    }
    ?>
    <div>Имя: <b><?= $model->name ?></b></div>
    
    <div>Телефон: <b><?= $model->telephone ?></b></div>
    
    <div>Дата: <b><?= Yii::$app->formatter->asDate($model->date_create) ?></b></div>
    
    <div>Email: <b><?= $model->email ?></b></div>
    
    <div>Вопрос: <b><?= $model->vopros ?></b></div>


    <?= $form->field($model, 'otvet')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Ответить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
