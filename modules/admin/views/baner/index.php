<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Банеры на сайте';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="baner-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить банер', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'imageFile',
                'value' => function ($data){
                    $img =$data->getImage();
                    return '<img src="/web/'.$img->getPath('x100').'" class="img-responsive" alt="">';
                },
                'format' => 'raw',
            ],
            'link',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
