<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datecontrol\DateControl;
use alex290\gallery\Gallery;

/* @var $this yii\web\View */
/* @var $model app\models\Gallery */
/* @var $form yii\widgets\ActiveForm */

$this->registerCss('.item-del { position: absolute; cursor: pointer;}');
?>

<div class="gallery-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
    <?php if ($model->isNewRecord): ?>
        <?= $form->field($model, 'images[]')->fileInput(['multiple' => true, 'accept' => 'images/*']) ?>
    <?php else: ?>
        <div class="row">
            <?php if (!$model->isNewRecord): ?>

                <div class="row">
                    <?= Gallery::widget(['modelsImages' => $model]) ?>  
                </div>

            <?php endif; ?>
        </div>
        <?= $form->field($model, 'images[]')->fileInput(['multiple' => true, 'accept' => 'images/*'])->label('Добавить изображение') ?>
    <?php endif; ?>


    <?=
    $form->field($model, 'date_create')->widget(DateControl::classname(), [
        'displayFormat' => 'php:d-m-Y',
        'type' => DateControl::FORMAT_DATE
    ])
    ?>

    <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
