<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model app\models\CategoryServices */

$this->title = 'Create Category Services';
$this->params['breadcrumbs'][] = ['label' => 'Category Services', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$mapCategoy = new \app\models\CategoryServices();
$parent = ArrayHelper::merge(['0' => 'Основная'], $mapCategoy->getTree());
?>
<div class="category-services-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'parent' => $parent,
        'model' => $model,
    ]) ?>

</div>
