<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CategoryServices */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsFile(
        '@web/web/lib/ckeditor/ckeditor.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);

$this->registerJsFile(
        '@web/js/ckeditor.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);

$this->registerJsFile(
        '@web/js/translitCyr.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);

?>

<div class="category-services-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true,
        'class' => 'input-in form-control']) ?>
    <?= $form->field($model, 'parent_id')->dropDownList($parent) ?>

    <?= $form->field($model, 'weight')->textInput() ?>

    <?= $form->field($model, 'description')->textarea(['class' => 'form-control text-full']) ?>

    <?= $form->field($model, 'alias')->textInput(['maxlength' => true,
        'class' => 'input-out form-control']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
