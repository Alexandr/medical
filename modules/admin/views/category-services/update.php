<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\CategoryServices */

$this->title = 'Update Category Services: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Category Services', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
$mapCategoy = new \app\models\CategoryServices();
$key = $mapCategoy->getTree();
unset($key[$model->id]);
$parent = ArrayHelper::merge(['0' => 'Основная'], $key);

?>
<div class="category-services-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'parent' => $parent,
        'model' => $model,
    ]) ?>

</div>
