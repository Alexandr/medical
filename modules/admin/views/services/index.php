<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\CategoryServices;
use app\components\pagesize\PageSize;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ServicesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Услуги';
$this->params['breadcrumbs'][] = $this->title;
$catServ = new CategoryServices;
$catServMap = $catServ->getTree();

$this->registerCss(".w-100 { width: 100px; }");
?>
<div class="services-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить услугу', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php echo PageSize::widget([
        'defaultPageSize' => 20,
        'sizes' => [20 => 20, 50 => 50, 100 => 100, 200 => 200],
        'label' => 'Количество материалов на странице',
        'options' => [
            'class' => 'form-control w-100',
        ],
    ]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'filterSelector' => 'select[name="per-page"]',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            [
                'attribute' => 'category_id',
                'value' => function ($data){
                    return $data->category->name;
                },
                'filter' => $catServMap,
            ],
            //'body:ntext',
            'date_create:date',
            'alias',
            'count',
            [
                'attribute' => 'user_id',
                'value' => function ($data) {
                    return $data->user->username;
                },
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    
</div>
