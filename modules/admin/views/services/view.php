<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Services */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Услуги', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="services-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ])
        ?>
    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'category.name',
            'name',
            'body:raw',
            'date_create:date',
            'alias',
            'count',
            [
                'attribute' => 'user_id',
                'value' => $model->user->username,
            ],
        ],
    ])
    ?>
    <div class="row">
        <h4>Расценки</h4>
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>№</th>
                    <th>Название</th>
                    <th>Стоимость</th>
                </tr>
            </thead>
            <tbody>
                <?php $contPr=0; foreach ($model->servicesPrices as $servicPrices): ?>
                <?php $contPr++; ?>
                <tr>
                    <td><?= $contPr ?></td>
                    <td><b><?= $servicPrices->name ?></b></td>
                    <td><?= $servicPrices->price ?> руб.</td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>

</div>
