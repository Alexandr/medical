<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datecontrol\DateControl;
use wbraganca\dynamicform\DynamicFormWidget;
use yii\helpers\ArrayHelper;
use app\models\CategoryServices;

/* @var $this yii\web\View */
/* @var $model app\models\Services */
/* @var $form yii\widgets\ActiveForm */
$catServ = new CategoryServices;
$catServMap = $catServ->getTree();
$this->registerJsFile(
        '@web/web/lib/ckeditor/ckeditor.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);

$this->registerJsFile(
        '@web/js/ckeditor.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);

$this->registerJsFile(
        '@web/js/translitCyr.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);
?>

<div class="services-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

    <?=
    $form->field($model, 'category_id')->dropDownList($catServMap, [
        'prompt' => 'Выберите категорию...',
        'options' => [
            '5' => [
                'selected' => false,
            ],
        ]
    ])
    ?>

    <?=
    $form->field($model, 'name')->textInput(['maxlength' => true,
        'class' => 'input-in form-control'])
    ?>

    <?= $form->field($model, 'body')->textarea(['class' => 'form-control text-full']) ?>

    <div class="row">
        <?php
        DynamicFormWidget::begin([
            'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
            'widgetBody' => '.container-items', // required: css class selector
            'widgetItem' => '.item', // required: css class
            'limit' => 100, // the maximum times, an element can be added (default 999)
            'min' => 0, // 0 or 1 (default 1)
            'insertButton' => '.add-item', // css class
            'deleteButton' => '.remove-item', // css class
            'model' => $modelsPrice[0],
            'formId' => 'dynamic-form',
            'formFields' => [
                'name',
                'price',
            ],
        ]);
        ?>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>
                    <i class="fa fa-money"></i> Расценки
                    
                </h4>
            </div>
            <div class="panel-body">
                <div class="container-items"><!-- widgetBody -->
                    <?php foreach ($modelsPrice as $i => $modelPrice): ?>
                        <div class="item panel panel-default"><!-- widgetItem -->
                            <div class="panel-heading">
                                <h3 class="panel-title pull-left"></h3>
                                <div class="pull-right">
                                    <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-body">
                                <?php
                                // necessary for update action.
                                if (!$modelPrice->isNewRecord) {
                                    echo Html::activeHiddenInput($modelPrice, "[{$i}]id");
                                }
                                ?>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <?= $form->field($modelPrice, "[{$i}]name")->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?= $form->field($modelPrice, "[{$i}]price")->textInput(['maxlength' => true]) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <button type="button" class="add-item btn btn-success btn-sm pull-right"><i class="glyphicon glyphicon-plus"></i> Добавить расценку</button>
            </div>
        </div><!-- .panel -->
        <?php DynamicFormWidget::end(); ?>
    </div>

    <?=
    $form->field($model, 'date_create')->widget(DateControl::classname(), [
        'displayFormat' => 'php:d-m-Y',
        'type' => DateControl::FORMAT_DATE
    ])
    ?>

    <?=
    $form->field($model, 'alias')->textInput(['maxlength' => true,
        'class' => 'input-out form-control'])
    ?>

    <?= $form->field($model, 'count')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'user_id')->hiddenInput()->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
