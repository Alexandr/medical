<?php

use yii\helpers\Html;



/* @var $this yii\web\View */
/* @var $model app\models\Services */

$this->title = 'Добавить услугу';
$this->params['breadcrumbs'][] = ['label' => 'Services', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$model->category_id=5;
$model->count=0;
$model->date_create = date('U');
$model->user_id = Yii::$app->user->identity->id;
?>
<div class="services-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelsPrice' => $modelsPrice,
    ]) ?>

</div>
