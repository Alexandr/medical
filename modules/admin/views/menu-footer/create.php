<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\MenuFooter;


/* @var $this yii\web\View */
/* @var $model app\models\MenuFooter */

$this->title = 'Create Menu Footer';
$this->params['breadcrumbs'][] = ['label' => 'Menu Footers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$mapMenuFooter = new MenuFooter();
$parent = ArrayHelper::merge(['0' => 'Основная'], $mapMenuFooter->getTree('map'));
?>
<div class="menu-footer-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'parent' => $parent,
        'model' => $model,
    ]) ?>

</div>
