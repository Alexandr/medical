<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\MenuFooter;

/* @var $this yii\web\View */
/* @var $model app\models\MenuFooter */

$this->title = 'Изменить ссылку меню подвала: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Меню подвала', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновить';

$mapMenuFooter = new MenuFooter();
$key = $mapMenuFooter->getTree('map');
unset($key[$model->id]);
$parent = ArrayHelper::merge(['0' => 'Основная'], $key);
?>
<div class="menu-footer-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'parent' => $parent,
        'model' => $model,
    ]) ?>

</div>
