<?php

use yii\helpers\Html;
use app\models\MenuTop;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model app\models\MenuTop */

$this->title = 'Добавить ссылку в верхнем меню';
$this->params['breadcrumbs'][] = ['label' => 'Верхнее меню', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$mapMenuTop = new MenuTop();
$parent = ArrayHelper::merge(['0' => 'Основная'], $mapMenuTop->getTree('map'));
?>
<div class="menu-top-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'parent' => $parent,
        'model' => $model,
    ]) ?>

</div>
