<?php

use yii\helpers\Html;
use app\models\MenuTop;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\MenuTop */

$this->title = 'Обновить ссылку: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Верхнее меню', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновить';

$mapMenuTop = new MenuTop();
$key = $mapMenuTop->getTree('map');
unset($key[$model->id]);
$parent = ArrayHelper::merge(['0' => 'Основная'], $key);
?>
<div class="menu-top-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'parent' => $parent,
        'model' => $model,
    ]) ?>

</div>
