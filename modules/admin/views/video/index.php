<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Видео';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="video-index">

    <h1><i class="fa fa-youtube" aria-hidden="true"></i> <?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить видео', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            // 'count',
            'name',
            'date_create:date',
            [
                'attribute' => 'video',
                'value' => function ($data) {
                    preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $data->video, $match);
                    return '<iframe width="200" height="100" src="https://www.youtube.com/embed/'.$match[1].'?rel=0" frameborder="0" allowfullscreen></iframe>';
                },
                'format' => 'raw',
            ],
            // 'alias',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
