<?php

/**
 * @var string $content
 * @var \yii\web\View $this
 */
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

use app\models\Vopros;

$voppros = Vopros::find()->where(['otvet' => null])->limit(6)->all();
$vopprosCount = Vopros::find()->where(['otvet' => null])->count();

$bundle = yiister\gentelella\assets\Asset::register($this);

$this->registerJsFile(
        'https://api-maps.yandex.ru/2.1/?lang=ru_RU'
);

$this->registerCssFile("@web/web/css/admin.css", [
    'depends' => [\yii\bootstrap\BootstrapAsset::className()]]);

?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta charset="<?= Yii::$app->charset ?>" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <?= Html::csrfMetaTags() ?>
        <link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl; ?>/web/favicon.png" type="image/x-icon" />
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="nav-<?= !empty($_COOKIE['menuIsCollapsed']) && $_COOKIE['menuIsCollapsed'] == 'true' ? 'sm' : 'md' ?>" >
        <?php $this->beginBody(); ?>
        <div class="container body">

            <div class="main_container">

                <div class="col-md-3 left_col">
                    <div class="left_col scroll-view">

                        <div class="navbar nav_title" style="border: 0;">
                            <a href="/admin" class="site_title"><span>Админ панель</span></a>
                        </div>
                        <div class="clearfix"></div>

                        <!-- menu prile quick info -->
                        <div class="profile">
                            <div class="profile_pic">
                                <img src="http://placehold.it/128x128" alt="..." class="img-circle profile_img">
                            </div>
                            <div class="profile_info">
                                <span>Добро пожаловать,</span>
                                <h2><?= Yii::$app->user->identity->username ?></h2>
                            </div>
                        </div>
                        <!-- /menu prile quick info -->

                        <br />

                        <!-- sidebar menu -->
                        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                            <div class="menu_section">
                                <p></p><p></p>
                                <?=
                                \yiister\gentelella\widgets\Menu::widget(
                                        [
                                            "items" => [
                                                ["label" => "Главная", "url" => "/", "icon" => "home"],
                                                ['label' => 'Категории', 'url' => ['/admin/category'], "icon" => "folder"],
                                                ['label' => 'Статьи', 'url' => ['/admin/article'], "icon" => "wpforms"],
                                                ['label' => 'Услуги', 'url' => '#', "icon" => "heartbeat",
                                                    "items" => [
                                                        ['label' => 'Категория услуг', 'url' => ['/admin/category-services'], "icon" => "stethoscope"],
                                                        ['label' => 'Услуги', 'url' => ['/admin/services'], "icon" => "heartbeat"],
                                                    ],
                                                ],
                                                ['label' => 'Врачи', 'url' => ['/admin/doctor'], "icon" => "user-circle-o"],
                                                ['label' => 'Адреса', 'url' => ['/admin/address'], "icon" => "map-marker"],
                                                ['label' => 'Слайдер на главной', 'url' => ['/admin/slider'], "icon" => "picture-o"],
                                                ['label' => 'Содержимое на главной', 'url' => ['/admin/front-content/view'], "icon" => "file-text-o"],
                                                ['label' => 'Меню', 'url' => '#', "icon" => "bars",
                                                    "items" => [
                                                        ['label' => 'Меню наверху', 'url' => ['/admin/menu-top'], "icon" => "bars"],
                                                        ['label' => 'Меню подвал', 'url' => ['/admin/menu-footer'], "icon" => "bars"],
                                                    ],
                                                ],
                                                ['label' => 'Вопрос', 'url' => ['/admin/vopros'], "icon" => "question-circle-o"],
                                                ['label' => 'Галерея', 'url' => ['/admin/gallery'], "icon" => "camera-retro"],
                                                ['label' => 'Видео', 'url' => ['/admin/video'], "icon" => "youtube"],
                                                ['label' => 'Отзывы', 'url' => ['/admin/comment'], "icon" => "comments-o"],
                                                ['label' => 'Банеры на сайте', 'url' => ['/admin/baner'], "icon" => "picture-o"],
                                                ['label' => 'URL ссылки', 'url' => ['/admin/sef'], "icon" => "link"],
                                            ],
                                        ]
                                )
                                ?>
                            </div>

                        </div>
                        <!-- /sidebar menu -->

                    </div>
                </div>

                <!-- top navigation -->
                <div class="top_nav">
                    
                    <div class="nav_menu">
                        <nav class="" role="navigation">
                            <div class="nav toggle">
                                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                            </div>

                            <ul class="nav navbar-nav navbar-right">
                                <li class="">
                                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <img src="http://placehold.it/128x128" alt=""><?= Yii::$app->user->identity->username ?>
                                        <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                                        <li><?= Html::a('Выход  <span class="fa fa-sign-out" aria-hidden="true"></span>', ['/site/logout'], ['data' => ['method' => 'post']]) ?>
                                        </li>
                                    </ul>
                                </li>

                                <li role="presentation" class="dropdown">
                                    <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                                        <i class="fa fa-envelope-o"></i>
                                        <span class="badge bg-green"><?= $vopprosCount ?></span>
                                    </a>
                                    <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                                        <?php foreach ($voppros as $vopr): ?>
                                        <li>
                                            <a>
                                                <span class="image">
                                                    <img src="http://placehold.it/128x128" alt="Profile Image" />
                                                </span>
                                                <span>
                                                    <span><?= $vopr->name ?></span>
                                                    <span class="time"><?= Yii::$app->formatter->asDate($vopr->date_create)?></span>
                                                </span>
                                                <span class="message">
                                                    <?= $vopr->vopros ?>
                                                </span>
                                            </a>
                                        </li>
                                        <?php endforeach; ?>
                                        <li>
                                            <div class="text-center">
                                                <?= Html::a('<strong>Посмотреть все вопросы</strong> <i class="fa fa-angle-right"></i>', ['/admin/vopros']) ?>
                                            </div>
                                        </li>
                                    </ul>
                                </li>

                            </ul>
                        </nav>
                    </div>

                </div>
                <!-- /top navigation -->

                <!-- page content -->
                <div class="right_col" role="main">
                     <?=
                    Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ])
                    ?>
                    <?php if (isset($this->params['h1'])): ?>
                        <div class="page-title">
                            <div class="title_left">
                                <h1><?= $this->params['h1'] ?></h1>
                            </div>
                            <div class="title_right">
                                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search for...">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="button">Go!</button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="clearfix"></div>

                    <?= $content ?>
                </div>
                <!-- /page content -->

            </div>

        </div>

        <div id="custom_notifications" class="custom-notifications dsp_none">
            <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
            </ul>
            <div class="clearfix"></div>
            <div id="notif-group" class="tabbed_notifications"></div>
        </div>
        <!-- /footer content -->
        <?php $this->endBody(); ?>
    </body>
</html>
<?php $this->endPage(); ?>
