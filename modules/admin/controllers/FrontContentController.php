<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\FrontContent;

/**
 * FrontContentController implements the CRUD actions for FrontContent model.
 */
class FrontContentController extends AdminController
{


    /**
     * Displays a single FrontContent model.
     * @param integer $id
     * @return mixed
     */
    public function actionView()
    {
        return $this->render('view', [
            'model' => FrontContent::findOne(1),
        ]);
    }



    /**
     * Updates an existing FrontContent model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate()
    {
        $model = FrontContent::findOne(1);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }


}
