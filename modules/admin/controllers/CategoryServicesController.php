<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\CategoryServices;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Sef;

/**
 * CategoryServicesController implements the CRUD actions for CategoryServices model.
 */
class CategoryServicesController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CategoryServices models.
     * @return mixed
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => CategoryServices::find(),
            'sort' => [
                'defaultOrder' => [
                    'weight' => SORT_ASC,
                ]
            ],
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }
    
//    public function actionSef() {
//        $models = CategoryServices::find()->all();
//        foreach ($models as $model) {
//            $sefModel = new \app\models\Sef;
//            $sefModel->link = 'category-services/view?id='.$model->id;
//            $sefModel->link_sef = $model->alias;
//            $sefModel->save();
//        }
//        return 'fsdsfgd';
//        
//    }

    /**
     * Displays a single CategoryServices model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CategoryServices model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new CategoryServices();
        $modelSef = new Sef();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $modelSef->link = 'category-services/view?id='.$model->id;
            $modelSef->link_sef = $model->alias;
            $modelSef->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CategoryServices model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $modelSef = Sef::findOne(['link' => 'category-services/view?id='.$model->id]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $modelSef->link = 'category-services/view?id='.$model->id;
            $modelSef->link_sef = $model->alias;
            $modelSef->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CategoryServices model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        
        $model = $this->findModel($id);
        $link = 'category-services/view?id='.$model->id;
        if (($modelSef = Sef::findOne(['link' => $link])) !== null) {
            $modelSef ->delete();
        }
        

        CategoryServices::deleteAll(['parent_id' => $id]);
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CategoryServices model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CategoryServices the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = CategoryServices::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
