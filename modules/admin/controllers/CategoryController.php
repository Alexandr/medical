<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Category;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Sef;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends AdminController {
    /**
     * @inheritdoc
     */

    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionIndex() {
        $query = Category::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'weight' => SORT_ASC,
                ]
            ],
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Category model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }
    
//    public function actionSef() {
//        $models = Category::find()->all();
//        foreach ($models as $model) {
//            $sefModel = new \app\models\Sef;
//            $sefModel->link = 'category/view?id='.$model->id;
//            $sefModel->link_sef = $model->alias;
//            $sefModel->save();
//        }
//        return 'fsdsfgd';
//        
//    }

    /**
     * Creates a new Category model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Category();
        $modelSef = new Sef();
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $modelSef->link = 'category/view?id='.$model->id;
            $modelSef->link_sef = $model->alias;
            $modelSef->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $modelSef = Sef::findOne(['link' => 'category/view?id='.$model->id]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $modelSef->link = 'category/view?id='.$model->id;
            $modelSef->link_sef = $model->alias;
            $modelSef->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Category model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        
        $model = $this->findModel($id);
        $link = 'category/view?id='.$model->id;
        if (($modelSef = Sef::findOne(['link' => $link])) !== null) {
            $modelSef ->delete();
        }
        
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
