<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Doctor;
use app\models\DoctorSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use app\models\Sef;

/**
 * DoctorController implements the CRUD actions for Doctor model.
 */
class DoctorController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Doctor models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new DoctorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

//    public function actionSef() {
//        $models = Doctor::find()->all();
//        foreach ($models as $model) {
//            $sefModel = new \app\models\Sef;
//            $sefModel->link = 'doctor/view?id=' . $model->id;
//            $sefModel->link_sef = 'doctor/' . $model->alias;
//            $sefModel->save();
//        }
//        return 'fsdsfgd';
//    }

    /**
     * Displays a single Doctor model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Doctor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Doctor();
        $modelSef = new Sef();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $modelSef->link = 'doctor/view?id='.$model->id;
            $modelSef->link_sef = 'doctor/'.$model->alias;
            $modelSef->save();
            $this->saveDoctorServ($model);
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            if ($model->imageFile) {
                $model->upload();
            }

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            $model->weight = Doctor::find()->count() + 1;
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Doctor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $model->services_array = $model->services;
        $modelSef = Sef::findOne(['link' => 'doctor/view?id='.$model->id]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $modelSef->link = 'doctor/view?id='.$model->id;
            $modelSef->link_sef = 'doctor/'.$model->alias;
            $modelSef->save();

            $this->updateDoctorServ($id, $model->services_array);


            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            if ($model->imageFile) {
                $model->removeImages();
                $model->upload();
            }

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Doctor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);
        $link = 'doctor/view?id='.$model->id;
        if (($modelSef = Sef::findOne(['link' => $link])) !== null) {
            $modelSef ->delete();
        }
        \app\models\DoctorService::deleteAll(['doctor_id' => $id]);
        $model->removeImages();
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Doctor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Doctor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Doctor::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function updateDoctorServ($id, $serv_arr) {

        $model = $this->findModel($id);
        $modelArr = $model->services;
        $arr = \yii\helpers\ArrayHelper::map($modelArr, 'id', 'id');
        if (is_array($serv_arr)) {
            foreach ($serv_arr as $one) {
                if (!in_array($one, $arr)) {
                    $modelDoctorServ = new \app\models\DoctorService();
                    $modelDoctorServ->doctor_id = $id;
                    $modelDoctorServ->services_id = $one;
                    $modelDoctorServ->save();
                }
                if (isset($arr[$one])) {
                    unset($arr[$one]);
                }
            }
        }
        \app\models\DoctorService::deleteAll(['services_id' => $arr]);
    }

    protected function saveDoctorServ($serv) {

        if (is_array($serv->services_array)) {
            foreach ($serv->services_array as $one) {
                $modelDoctorServ = new \app\models\DoctorService();
                $modelDoctorServ->doctor_id = $serv->id;
                $modelDoctorServ->services_id = $one;
                $modelDoctorServ->save();
            }
        }
    }

}
