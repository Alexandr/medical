<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Article;
use app\models\ArticleSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Sef;

/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticleController extends AdminController
{
    /**
     * @inheritdoc
     */
//    public function behaviors()
//    {
//        return [
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'delete' => ['POST'],
//                ],
//            ],
//        ];
//    }

    /**
     * Lists all Article models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Article model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Article model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Article();
        $modelSef = new Sef();
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $modelSef->link = 'article/view?id='.$model->id;
            $modelSef->link_sef = $model->alias;
            $modelSef->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
//    public function actionSef() {
//        $models = \app\models\Article::find()->all();
//        foreach ($models as $model) {
//            $sefModel = new \app\models\Sef;
//            $sefModel->link = 'article/view?id='.$model->id;
//            $sefModel->link_sef = $model->alias;
//            $sefModel->save();
//        }
//        return 'fsdsfgd';
//        
//    }

    /**
     * Updates an existing Article model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelSef = Sef::findOne(['link' => 'article/view?id='.$model->id]);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $modelSef->link = 'article/view?id='.$model->id;
            $modelSef->link_sef = $model->alias;
            $modelSef->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Article model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $link = 'article/view?id='.$model->id;
        if (($modelSef = Sef::findOne(['link' => $link])) !== null) {
            $modelSef ->delete();
        }
        
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Article model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Article the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Article::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
