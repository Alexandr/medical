<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Services;
use app\models\ServicesSearch;
use app\models\ServicesPrice;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Model;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Sef;
//use dastanaron\translit\Translit;

/**
 * ServicesController implements the CRUD actions for Services model.
 */
class ServicesController extends AdminController
{
    /**
     * @inheritdoc
     */

    /**
     * Lists all Services models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ServicesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Services model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
//    public function actionSef() {
//        $models = Services::find()->all();
//        foreach ($models as $model) {
//            $sefModel = new \app\models\Sef;
//            $sefModel->link = 'services/view?id='.$model->id;
//            $sefModel->link_sef = $model->category->alias.'/'.$model->alias;
//            $sefModel->save();
//        }
//        return 'fsdsfgd';
//        
//    }

    /**
     * Creates a new Services model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Services();
        $modelsPrice = [new ServicesPrice];
        $modelSef = new Sef();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            $translit = new Translit();
//            $model->alias = $translit->translit($model->name, true, 'ru-en');
            $modelSef->link = 'services/view?id='.$model->id;
            $modelSef->link_sef = $model->category->alias.'/'.$model->alias;
            $modelSef->save();
            
            $modelsPrice = Model::createMultiple(ServicesPrice::classname());
            Model::loadMultiple($modelsPrice, Yii::$app->request->post());

            // ajax validation
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ArrayHelper::merge(
                    ActiveForm::validateMultiple($modelsPrice),
                    ActiveForm::validate($model)
                );
            }

            // validate all models
            $valid = $model->validate();
            // $valid = Model::validateMultiple($modelsPrice) && $valid;
            
            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        foreach ($modelsPrice as $modelPrice) {
                            $modelPrice->services_id = $model->id;
                            if (! ($flag = $modelPrice->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
            
        } else {
            return $this->render('create', [
                'model' => $model,
                'modelsPrice' => (empty($modelsPrice)) ? [new ServicesPrice] : $modelsPrice,
            ]);
        }
    }

    /**
     * Updates an existing Services model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelsPrice = $model->servicesPrices;
        $modelSef = Sef::findOne(['link' => 'services/view?id='.$model->id]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $modelSef->link = 'services/view?id='.$model->id;
            $modelSef->link_sef = $model->category->alias.'/'.$model->alias;
            $modelSef->save();
            
            $oldIDs = ArrayHelper::map($modelsPrice, 'id', 'id');
            $modelsPrice = Model::createMultiple(ServicesPrice::classname(), $modelsPrice);
            Model::loadMultiple($modelsPrice, Yii::$app->request->post());
            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsPrice, 'id', 'id')));

            // ajax validation
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ArrayHelper::merge(
                    ActiveForm::validateMultiple($modelsPrice),
                    ActiveForm::validate($model)
                );
            }

            // validate all models
            $valid = $model->validate();
            // $valid = Model::validateMultiple($modelsPrice) && $valid;

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        if (! empty($deletedIDs)) {
                            ServicesPrice::deleteAll(['id' => $deletedIDs]);
                        }
                        foreach ($modelsPrice as $modelPrice) {
                            $modelPrice->services_id = $model->id;
                            if (! ($flag = $modelPrice->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
            
        } else {
            return $this->render('update', [
                'model' => $model,
                'modelsPrice' => (empty($modelsPrice)) ? [new ServicesPrice] : $modelsPrice,
            ]);
        }
    }

    /**
     * Deletes an existing Services model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        
        $model = $this->findModel($id);
        $link = 'services/view?id='.$model->id;
        if (($modelSef = Sef::findOne(['link' => $link])) !== null) {
            $modelSef ->delete();
        }
        
        ServicesPrice::deleteAll(['services_id' => $id]);
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Services model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Services the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Services::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
