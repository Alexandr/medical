<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CategoryServices */

$this->title = $model->name;
$this->params['breadcrumbs'][] = $this->title;
$services = $model->services;
$fronContent = \app\models\FrontContent::findOne(1);
$baners = \app\models\Baner::find()->all();
?>
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-3 right-bar">
            <div class="baner-wrap">
                <?php foreach ($baners as $baner): ?><p>
                        <a href="<?= $baner->link ?>" target="_blank">
                            <img src="/web/<?= $baner->getImage()->getPath() ?>" class="img-responsive" alt="">
                        </a>
                        <br></p>
                <?php endforeach; ?>
            </div>
            <?= app\components\accordion\AccordionMenu::widget(['categor' => $model->id]) ?>
        </div>
        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-9 content">
            <div class="row">
                <h1 class="text-center"><?= Html::encode($this->title) ?></h1>
            </div>
            <p></p>

            <div class="row">
                <div class="content"><?= $model->description ?></div>
            </div>
        </div>
    </div>
</div>
