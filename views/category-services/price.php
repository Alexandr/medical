<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CategoryServices */

$this->title = $model->name;
$this->params['breadcrumbs'][] = $this->title;
$services = $model->services;
$fronContent = \app\models\FrontContent::findOne(1);
$baners = \app\models\Baner::find()->all();
?>
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-3 right-bar">
            <div class="baner-wrap">
                <?php foreach ($baners as $baner): ?><p>
                        <a href="<?= $baner->link ?>" target="_blank">
                            <img src="/web/<?= $baner->getImage()->getPath() ?>" class="img-responsive" alt="">
                        </a>
                        <br></p>
                <?php endforeach; ?>
            </div>
            <?= app\components\accordion\AccordionMenu::widget(['categor' => $model->id]) ?>
        </div>
        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-9 content">
            <div class="row">
                <h1 class="green-link text-center"><?= Html::a($this->title, ['/category-services/view', 'id'=>$model->id]) ?></h1>
            </div><div class="row text-right">
                <?= Html::a('<i class="fa fa-file-excel-o" aria-hidden="true"></i> Скачать прайс', '/site/price', ['class' => 'btn btn-default']) ?>
            </div>
            <p></p>
            <div class="row">
                <table class="table table-bordered table-hover">

                    <tbody>
                        <?php foreach ($services as $service): ?>
                            <tr>
                                <td colspan="3">
                                    <h3 class="text-center uslug-price">
                                        <?= Html::a($service->name, ['/services/view', 'id' => $service->id]) ?>
                                    </h3>
                                </td>
                            </tr>
                            <?php $contPr = 0;
                            foreach ($service->servicesPrices as $servic): ?>
        <?php $contPr++; ?>
                                <tr>
                                    <td><?= $contPr ?></td>
                                    <td><b><?= $servic->name ?></b></td>
                                    <td class="w-115"><?= $servic->price ?> руб.</td>
                                </tr>
                            <?php endforeach; ?>
<?php endforeach; ?>

                    </tbody>
                </table>

            </div>
            <div class="row">
                <div class="content"><?= $model->description ?></div>
            </div>
        </div>
    </div>
</div>
