<?php
/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\MenuTop;
use app\models\Services;
use app\models\Address;
use app\models\Search;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Url;

$search = new Search();

AppAsset::register($this);
$this->registerJsFile(
        'https://api-maps.yandex.ru/2.1/?lang=ru_RU'
);
$fronCont = \app\models\FrontContent::findOne(1);
$menuTop = new MenuTop();
$menuTopMap = $menuTop->getTree('tree');
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Медицинский центр «Здоровье» - Клиника &quot;Здоровье&quot; в Махачкале. Клиника здоровье Махачкала',
]);

$this->registerMetaTag([
    'name' => 'keywords',
    'content' => 'Медицинский центр, центр Здоровье, Клиника Здоровье, поликлинка в Махачкале, Клиника здоровье Махачкала',
]);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl;
        ?>/web/favicon.png" type="image/x-icon" />
              <?php $this->head() ?>

        <!--[if lt IE 9]>
        <script src="/web/js/html5shiv.min.js"></script>
        <script src="/web/js/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <?php $this->beginBody() ?>



        <div class="page-wrap">
            <div class="header-wr fl-w">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-5 col-sm-6 col-md-3 col-lg-3">
                            <div class="logo"><a href="/">
                                    <img src="/images/logo.png" alt="" height="150px" width="auto"></a></div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-2 col-lg-2 text-center top-20">
                            <p></p>
                            <p></p>
                            <p>
                                <!-- Split button -->
                            <div class="btn-group">
                                <?= Html::a('Прайс-лист', '/otdelenia/price', ['class' => 'btn btn-default']) ?>
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><?= Html::a('Открыть прайс', ['category-services/price', 'id' => 1]) ?></li>
                                    <li><?= Html::a('Скачать прайс', '/site/price') ?></li>
                                </ul>
                            </div>
                            </p>
                            <p>
                                <a href="https://docs.google.com/spreadsheets/d/1t_h-cgtr6P6V8TdRU2WGLU1fkL1aDggQESmfr_J8Obc/edit?usp=sharing" target="_blank" class="btn btn-default">График врачей</a>
                            </p>
                            <p>
                                <a href="<?= Url::to(['/article/view', 'id'=>279]) ?>" target="_blank" class="btn btn-default">ОМС</a>
                            </p>
                        </div>
                        <div class="col-xs-12 col-sm-12 line-hiden"></div>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <div class="grafic">
                                <h3><a href="https://docs.google.com/spreadsheets/d/1t_h-cgtr6P6V8TdRU2WGLU1fkL1aDggQESmfr_J8Obc/edit?usp=sharing" target="_blank">График работы:</a></h3>
                                <p class="fa fa-clock-o icon" aria-hidden="true">
                                <p class="text"><?= $fronCont->graphik ?></p>
                                </p>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                            <div class="top-tel">
                                <h3>Телефоны для справок:</h3>
                                <p class="fa fa-volume-control-phone">
                                    <span><a href="tel:<?= $fronCont->telephone ?>"><?= $fronCont->telephone ?></a></span>
                                </p>
                            </div>
                            <div class="inst-icon text-right">
                                <a href="https://www.instagram.com/klinika_zdorovie/" target="_blank">
                                    <img src="/web/images/instagram.png"  width="25" height="auto" alt=""> Здоровье</a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 line-hiden"></div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-sm-offset-0 col-md-offset-6 col-lg-offset-6">
                            <div class="top-search">
                                <?php $form = ActiveForm::begin(); ?>
                                <?= $form->field($search, 'search')->textInput(['class' => 'form-control'])->label(FALSE) ?>
                                <?= Html::submitButton('', ['class' => 'btn btn-default fa fa-search']) ?>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
            <nav class="navbar navbar-default nav-green fl-w" role="navigation">
                <div class="container"><div class="row">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse navbar-ex1-collapse">
                            <?= app\components\NavHover::widget(['menu' => 'top']) ?>
                        </div>
                        <!-- /.navbar-collapse -->
                    </div></div>
            </nav>
            <div class="header-wr fl-w mobile-w">
                <div class="container">
                    <div class="rov">
                        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                            <div class="logo"><a href="/">
                                    <img src="/images/logo.png" alt="" height="150px" width="auto"></a></div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-1 col-lg-2 text-center top-50">
                            <p>
                                <!-- Split button -->
                            <div class="btn-group">
                                <?= Html::a('Прайс-лист', '/category-services/view?id=1', ['class' => 'btn btn-default']) ?>
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><?= Html::a('Открыть прайс', '/category-services/view?id=1') ?></li>
                                    <li><?= Html::a('Скачать прайс', '/site/price') ?></li>
                                </ul>
                            </div>
                            </p>
                            <p>
                                <a href="https://docs.google.com/spreadsheets/d/1t_h-cgtr6P6V8TdRU2WGLU1fkL1aDggQESmfr_J8Obc/edit?usp=sharing" target="_blank" class="btn btn-default">График врачей</a>
                            </p>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <div class="grafic">
                                <h3>График работы:</h3>
                                <p class="fa fa-clock-o icon" aria-hidden="true">
                                <p class="text">Пн-Пт с 9:00. по 18:00</p>
                                <p class="text">Сб-Вс с 9:00. по 16:00</p>
                                </p>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                            <div class="top-tel">
                                <h3>Телефоны для справок:</h3>
                                <p class="fa fa-volume-control-phone">
                                    <span><a href="tel:<?= $fronCont->telephone ?>"><?= $fronCont->telephone ?></a></span>
                                </p>
                            </div>
                            <div class="inst-icon text-center">
                                <a href="https://www.instagram.com/klinika_zdorovie/" target="_blank">
                                    <img src="/web/images/instagram.png"  width="25" height="auto" alt=""> Здоровье</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="container">
                <?=
                Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ])
                ?>
                <?= Alert::widget() ?>
            </div>
            <?= $content ?>

            <footer id="footer" class="footer-wrap fl-w">
                <div class="container">
                    <div class="row">
                        <nav class="navbar navbar-default nav-green fl-w" role="navigation">
                            <div class="container">
                                <div class="row">
                                    <!-- Brand and toggle get grouped for better mobile display -->
                                    <div class="navbar-header">
                                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex2-collapse">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                                    </div>
                                    <!-- Collect the nav links, forms, and other content for toggling -->
                                    <div class="collapse navbar-collapse navbar-ex2-collapse">
                                        <?= app\components\NavHover::widget(['menu' => 'footer']) ?>
                                    </div>
                                </div>
                                <!-- /.navbar-collapse -->
                            </div>
                        </nav>
                    </div>
                </div>
                <div class="container footer__info">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                            <a href="/" class="footer__logo logo"> <img width="200px" height="auto" src="/images/logo.png"> <p></p></a>
                            <div class="footer__text">
                                <p></p>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-8 col-lg-8">
                            <div class="footer__address">
                                <p class="footer__address--caption">Адреса наших клиник:</p>
                                <?php
                                $addr = Address::find()->all();
                                foreach ($addr as $valueAddres):
                                    ?>
                                    <div class="footer__address--list clearfix">
                                        <div>
                                            <p class="footer__adress--city item-adr" data="<?= $valueAddres->id ?>">
                                                <span><i class="fa fa-map-marker" aria-hidden="true"></i> &nbsp;<?= $valueAddres->address ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                <span> <i class="fa fa-phone" aria-hidden="true"></i> <?= $valueAddres->telephon ?></span></p>     
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <?php foreach ($addr as $valueAddr): ?>
                                <div class="map-wrap map-wr<?= $valueAddr->id ?>" data="<?= $valueAddr->id ?>">
                                    <div id="mapfr<?= $valueAddr->id ?>" class="map-adr" data="<?= $valueAddr->id ?>">
                                        <div class="hide map-text"><?= $valueAddr->map ?></div>
                                    </div></div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="container center_block">
                        <div class="footer-menuMD">
                            <div class="row">
                                <h3 class="footer-menuMD-title">Категории услуг:</h3>
                                <ul class="nav menufootermenu">
                                    <?php foreach (Services::find()->where(['category_id' => 1])->all() as $valueServ): ?>
                                        <li><a href="<?= yii\helpers\Url::to(['/services/view', 'id' => $valueServ->id]) ?>"><?= $valueServ->name ?></a></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer__meta">
                    <div class="container">
                        <div class="row">
                            <p class="col-md-10 footer__copyright"> <a class="sitename" href="/" title="Медицинский центр –  «Целитель», сеть аптек и поликлиник">Medical Diagnostic Center © 2017 Privacy Policy</a>    <a class="sitemap fl-right" href="/sitemap" title="Карта сайта"><span class="fa fa-sitemap"></span> Карта сайта</a> </p>
                            <div class="li col-md-2">
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>

        <?php $this->endBody() ?>
		<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter47153562 = new Ya.Metrika({
                    id:47153562,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/47153562" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
    </body>
</html>
<?php $this->endPage() ?>