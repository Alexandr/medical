<?php
/* @var $this yii\web\View */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\models\Comment;

$this->title = 'Отзывы посетителей';

$comments = Comment::find()->orderBy(['date_create' => SORT_DESC])->all();
?>
<div class="container">
    <div class="row">
        <h1 class="text-center"><?= $this->title ?></h1>
    </div>
    <?php foreach ($comments as $comment): ?>

        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="right-time">
                    <i class="fa fa-calendar" aria-hidden="true"></i> <?= Yii::$app->formatter->asDate($comment->date_create) ?>
                </div>
                <h2 class="panel-title"><?= $comment->name ?></h2>
            </div>
            <div class="panel-body">
                <?= $comment->comment ?>
            </div>
        </div>
        <p></p>
    <?php endforeach; ?>
    <div class="row text-center">
        <a class="btn btn-default" data-toggle="modal" href='#modal-comment'>Оставить отзыв</a>
    </div>
    <div class="modal fade bs-example-modal-lg" id="modal-comment">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Оставить отзыв</h4>
                </div>
                <div class="modal-body">
                    <?php $form = ActiveForm::begin(); ?>
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Ваше имя'])->label(false) ?>

                    <?= $form->field($model, 'telephone')->textInput(['maxlength' => true, 'placeholder' => 'Ваш телефон'])->label(false) ?>

                    <?= $form->field($model, 'date_create')->hiddenInput(['value' => date('U')])->label(false) ?>

                    <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'placeholder' => 'Ваш Email'])->label(false) ?>

                    <?= $form->field($model, 'title')->textInput(['maxlength' => true, 'placeholder' => 'Заголовок'])->label(false) ?>

                    <?= $form->field($model, 'comment')->textarea(['rows' => 6, 'placeholder' => 'Ваш отзыв'])->label(false) ?>

                    <div class="form-group text-center">
                        <?= Html::submitButton('Отправить', ['class' => 'btn btn-success']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>


</div>
