<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DoctorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Врачи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-3 right-bar">
            <?php echo $this->render('_search', ['model' => $searchModel]); ?>
        </div>

        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-9 content-reght">
            


                <h1 class="text-center"><?= Html::encode($this->title) ?></h1>
                <div class="row">


                <?=
                ListView::widget([
                    'dataProvider' => $dataProvider,
                    'itemOptions' => ['class' => 'item'],
                    'itemView' => '_doctor',
                    'summary' => '',
                    'layout' => '<div class="row grid">{items}</div><div class="row">{pager}</div>'
                ])
                ?>
            </div>
                
                
                
        </div>
    </div>
</div>
