<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Doctor */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Врачи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row">
        <img style="float: left; margin: 20px" src="/web/<?= $model->getImage()->getPath('300x300') ?>" alt="">
        <div class="cont-doctor"><?= $model->body ?></div>
    </div>
    <div class="row">
        <h2 class="text-center green">Задать вопрос</h2>
        <?php $form = ActiveForm::begin(); ?>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
            <?= $form->field($modelVopros, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
            <?= $form->field($modelVopros, 'telephone')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
            <?= $form->field($modelVopros, 'email')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <?= $form->field($modelVopros, 'vopros')->textarea(['rows' => 6]) ?>
        </div>

        <?= $form->field($modelVopros, 'doctor_id')->hiddenInput(['value' => $model->id])->label(FALSE) ?>

        <?= $form->field($modelVopros, 'date_create')->hiddenInput(['value' => date('U')])->label(FALSE) ?>

        <div class="form-group">
            <div class="text-center"><?= Html::submitButton('Отправить', ['class' => 'btn btn-success']) ?></div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
