<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
?>
<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 doctor grid-item">
    <div class="doctor-wrap">
        <div class="imges">
            <img class="img-responsive" src="/web/<?= $model->getImage()->getPath('600x600') ?>" alt="">
        </div>
        <h3 class="title-doc"><?= Html::a($model->name, ['/doctor/view', 'id' => $model->id]) ?></h3>
        <div class="opis-doctor">
            <?php foreach ($model->services as $serviceDoc): ?>
                <p><?= $serviceDoc->name ?></p>
            <?php endforeach; ?>
        </div>
        <?= Html::a('Подробнее', ['/doctor/view', 'id' => $model->id], ['class' => 'btn btn-success btn-block']) ?>

    </div>
</div>