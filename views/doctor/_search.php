<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\CategoryServices;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use app\models\Services;

$servic = new Services();

$modelServise = new CategoryServices();
$dataArray = [];

$modelServiseArray = $modelServise->getTree();

foreach ($modelServiseArray as $key => $valServCat) {
    $name = strval($modelServiseArray[$key]);
    $dataArray = ArrayHelper::merge($dataArray, array($name => $name));
    $dataArraySub = [];
    $dataArraySevs = ArrayHelper::map(Services::find()->where(['category_id' => $key])->all(), 'id', 'name');
    foreach ($dataArraySevs as $key => $dataArraySev) {
        $countDoctor = app\models\Doctor::find()->joinWith('services')->where(['services.id' => $key])->count();
        $dataArraySub = ArrayHelper::merge($dataArraySub, [$key => $dataArraySev . ' (' . $countDoctor . ')']);
    }
    $dataArray[$name] = $dataArraySub;
}



/* @var $this yii\web\View */
/* @var $model app\models\DoctorSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="doctor-search">

    <?php
    $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
    ]);
    ?>
    <div class="scoreboard">
        <h3><i class="fa fa-heartbeat" aria-hidden="true"></i>  Подбор врача:</h3>
    </div><p></p>

    <?=
    $form->field($model, 'servicesId')->widget(Select2::classname(), [
        'data' => $dataArray,
        'language' => 'ru',
        'options' => [
            'placeholder' => 'Выберете услугу ...',
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ])->label('Услуги')
    ?>

    <?= $form->field($model, 'name')->label('Имя врача') ?>

    <?php // $form->field($model, 'body') ?>

<?php // $form->field($model, 'date_create')  ?>

    <?php // $form->field($model, 'count')  ?>




    <?php // echo $form->field($model, 'alias') ?>

        <?php // echo $form->field($model, 'weight')  ?>

        <?php // echo $form->field($model, 'user_id') ?>

    <div class="form-group">
    <?= Html::submitButton('Подобрать', ['class' => 'btn btn-success']) ?> 
<?= Html::a('Сбросить', '/doctor', ['class' => 'btn btn-default']) ?>
    </div>

<?php ActiveForm::end(); ?>

</div>
