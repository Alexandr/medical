<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Category;

/* @var $this yii\web\View */
/* @var $model app\models\Article */
$category = Category::findOne($model->category_id);

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => $category->name, 'url' => [\yii\helpers\Url::to(['/category/view', 'id' => $category->id])]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">

    <h1 class="text-center"><?= Html::encode($this->title) ?></h1>

   <div class="row">
       <?= $model->body ?>
   </div>

</div>
