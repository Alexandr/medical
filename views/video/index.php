<?php

use yii\helpers\Html;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Видео';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container video-index">
    <div class="row grid">
        <?php foreach ($models as $model): ?>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 grid-item">
                <h4 class="post-article"><?= Html::a($model->name, ['/video/view', 'id' => $model->id]) ?></h4>
                <div class="video">
                    <?php if (!empty($model->video)): ?>
                        <?php if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $model->video, $match)): ?>
                            <div class="w-480">
                                <div class="video-container">
                                    <iframe width="560" height="315" src="https://www.youtube.com/embed/<?= $match[1] ?>?rel=0" frameborder="0" allowfullscreen></iframe>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    <div class="row">
        <?php
// отображаем постраничную разбивку
        echo LinkPager::widget([
            'pagination' => $pages,
            'registerLinkTags' => true
        ]);
        ?>
    </div>
</div>
