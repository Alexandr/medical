<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Video */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Видео', 'url' => ['/video']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container video-view">

    <h1><?= Html::encode($this->title) ?></h1>


    <div class="video">
        <?php if (!empty($model->video)): ?>
            <?php if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $model->video, $match)): ?>
                <div class="w-480">
                    <div class="video-container">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/<?= $match[1] ?>?rel=0" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            <?php endif; ?>
        <?php endif; ?>
    </div>


</div>
