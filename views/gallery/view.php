<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Gallery */

$this->title = $model->name;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div id="basicExample">
            <?php $images = $model->getImages(); foreach ($images as $image): ?>
            <a class="color-box-gr" href="/web/<?= $image->getPath() ?>">
                    <img src="/web/<?= $image->getPath() ?>"/>
                </a>
            <?php endforeach; ?>
        </div>
    </div>

</div>
