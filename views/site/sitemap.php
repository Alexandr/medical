<?php
use app\models\Article;
use yii\helpers\Url;
use app\models\Category;
use app\models\CategoryServices;
use app\models\Services;
use app\models\Doctor;


$articles = Article::find()->all();
$categories = Category::find()->all();
$domen = 'http://xn----dtbebcbdn4bnoavey5d7f.xn--p1ai';
?>
<url>
        <loc><?= $domen ?></loc>
        <priority>0.5</priority>
    </url>
<?php foreach ($categories as $category):?>
    <url>
        <loc><?= $domen.Url::to(['/category/view' , 'id' => $category->id])?></loc>
        <priority>0.5</priority>
    </url>
<?php endforeach; ?>
<?php foreach ($articles as $article):?>
    <url>
        <loc><?= $domen.Url::to(['/article/view' , 'id' => $article->id])?></loc>
        <priority>0.5</priority>
    </url>
<?php endforeach; ?>
<?php foreach (CategoryServices::find()->all() as $conten):?>
    <url>
        <loc><?= $domen.Url::to(['/category-services/view' , 'id' => $conten->id])?></loc>
        <priority>0.5</priority>
    </url>
<?php endforeach; ?>
<?php foreach (Services::find()->all() as $conten):?>
    <url>
        <loc><?= $domen.Url::to(['/services/view' , 'id' => $conten->id])?></loc>
        <priority>0.5</priority>
    </url>
<?php endforeach; ?>
<?php foreach (Doctor::find()->all() as $conten):?>
    <url>
        <loc><?= $domen.Url::to(['/doctor/view' , 'id' => $conten->id])?></loc>
        <priority>0.5</priority>
    </url>
<?php endforeach; ?>