<?php

use app\models\CategoryServices;
use app\models\Services;
use app\models\ServicesPrice;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;

$countSheet = 0;


// Create new Spreadsheet object
$spreadsheet = new Spreadsheet();

$category = new CategoryServices();
$categorieMap = $category->getTree();

// Set document properties
$spreadsheet->getProperties()->setCreator('Maarten Balliauw')
        ->setLastModifiedBy('Maarten Balliauw')
        ->setTitle('Office 2007 XLSX Test Document')
        ->setSubject('Office 2007 XLSX Test Document')
        ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
        ->setKeywords('office 2007 openxml php')
        ->setCategory('Price result file');



foreach ($categorieMap as $key => $categoriyMap) {
    $count = 1;
    if ($countSheet == 0) {
        $spreadsheet->setActiveSheetIndex(0);
    } else {
        $spreadsheet->createSheet();
        $spreadsheet->setActiveSheetIndex($countSheet);
    }
    $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth('5');
    $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth('100');
    $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth('20');
    $spreadsheet->getActiveSheet()->setTitle($categoriyMap);
    
    $spreadsheet->setActiveSheetIndex($countSheet)->mergeCells('A1:C1');
    $spreadsheet->setActiveSheetIndex($countSheet)
                ->setCellValue('A1', 'Прайс: '.mb_strtolower($categoriyMap));
    $spreadsheet->getActiveSheet()->getStyle('A' . $count)
                ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('A' . $count)
                ->getFont()->setSize('24');
    

    $services = Services::find()->where(['category_id' => $key])->all();
    foreach ($services as $service) {
        $count++;


        $spreadsheet->setActiveSheetIndex($countSheet)->mergeCells('A' . $count . ':C' . $count);
        $spreadsheet->setActiveSheetIndex($countSheet)
                ->setCellValue('A' . $count, $service->name);
        $spreadsheet->getActiveSheet()->getStyle('A' . $count)
                ->getFont()->getColor()->setARGB('FFFFFFFF');
        $spreadsheet->getActiveSheet()->getStyle('A' . $count)
                ->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
        $spreadsheet->getActiveSheet()->getStyle('A' . $count)
                ->getFill()->getStartColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_DARKGREEN);
        $spreadsheet->getActiveSheet()->getStyle('A' . $count)
                ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('A' . $count)
                ->getFont()->setSize('18');




        $servPrices = ServicesPrice::find()->where(['services_id' => $service->id])->all();
        $prCouunt = 0;
        foreach ($servPrices as $servPrice) {
            $count++;
            $prCouunt++;
            $spreadsheet->setActiveSheetIndex($countSheet)
                    ->setCellValue('A' . $count, $prCouunt);
            $spreadsheet->getActiveSheet()->getStyle('A' . $count)
                    ->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $spreadsheet->getActiveSheet()->getStyle('B' . $count)
                    ->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $spreadsheet->getActiveSheet()->getStyle('C' . $count)
                    ->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $spreadsheet->getActiveSheet()->getStyle('B' . $count)
                    ->getAlignment()->setWrapText(TRUE);
            $spreadsheet->setActiveSheetIndex($countSheet)
                    ->setCellValue('B' . $count, $servPrice->name);
            $spreadsheet->setActiveSheetIndex($countSheet)
                    ->setCellValue('C' . $count, $servPrice->price . ' руб.');
        }
        $count++;
        $count++;
    }
    $countSheet++;
}

$spreadsheet->setActiveSheetIndex(0);

/* $spreadsheet->setActiveSheetIndex(0)
  ->setCellValue('A1', 'Hello')
  ->setCellValue('B2', 'world!')
  ->setCellValue('C1', 'Hello')
  ->setCellValue('D2', 'world!');

  // Miscellaneous glyphs, UTF-8
  $spreadsheet->setActiveSheetIndex(0)
  ->setCellValue('A4', 'fgdfgd')
  ->setCellValue('A5', 'ddfgfdfgfdgfdg'); */

// Rename worksheet
/* $spreadsheet->getActiveSheet()->setTitle('Simple');

  // Set active sheet index to the first sheet, so Excel opens this as the first sheet
  $spreadsheet->setActiveSheetIndex(0);

  $spreadsheet->createSheet();
  $spreadsheet->setActiveSheetIndex(1);
  $spreadsheet->getActiveSheet()->setTitle('Simple'); */



// Redirect output to a client’s web browser (Xlsx)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="price.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0

$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
$writer->save('php://output');
?>