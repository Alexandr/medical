<?php

use yii\helpers\Html;
use yii\data\ActiveDataProvider;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $model app\models\Category */

$this->title = 'Поиск по слову ' . $q;
$this->params['breadcrumbs'][] = $this->title;

$dataProvider = new ActiveDataProvider([
    'query' => $modelServices,
    'pagination' => [
        'pageSize' => 20,
    ],
    'sort' => [
        'defaultOrder' => [
            'date_create' => SORT_DESC,
        ]
    ],
        ]);
?>
<div class="container">
    <div class="row">
        <h1 class="text-center"><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="row">
        
        <?=
        ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_postS',
            'summary' => '',
        ]);
        ?>
        <?php
        $dataProvider = new ActiveDataProvider([
            'query' => $modelCategoryServices,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => [
                'defaultOrder' => [
                    'weight' => SORT_DESC,
                ]
            ],
        ]);
        ?>
        <?=
        ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_postC',
            'summary' => '',
        ]);
        ?>
        <?php
        $dataProvider = new ActiveDataProvider([
            'query' => $model,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => [
                'defaultOrder' => [
                    'date_create' => SORT_DESC,
                ]
            ],
        ]);
        ?>
        <?=
        ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_post',
            'summary' => '',
        ]);
        ?>
    </div>
</div>
