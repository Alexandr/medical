<?php
/* @var $this yii\web\View */

use app\models\Slider;
use app\models\Services;

$this->title = 'Медицинский центр «Здоровье» в Махачкале';
$slide = Slider::find()->all();
$fronContent = \app\models\FrontContent::findOne(1);
$services = Services::find()->all();

?>

<div class="slider-wrap fl-w">
    <div class="flexslider top">
        <ul class="slides">
            <?php foreach ($slide as $slideVal): ?>
                <li>
                    <div class="slide-title">
                        <div class="container">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-md-offset-6 col-lg-offset-6">
                                <div class="text-slide">
                                    <h2><?= $slideVal->name ?></h2>
                                    <p><?= $slideVal->body ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <img src="/web/<?= $slideVal->getImage()->getPath('2048x490') ?>" alt=""></li>
            <?php endforeach; ?>
        </ul>
    </div>

</div>
<div class="content-wrap fl-w">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-3 right-bar">
                <div class="scoreboard">
                    <h3 class="fa-clock-o">  График работы:</h3>
                    <time class="h6"><?= $fronContent->graphik ?></time>
                </div>
                <?= app\components\accordion\AccordionMenu::widget() ?>
            </div>
            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-9 content"><div class="row">
                    <div class="flexslider news-front">
                        <ul class="slides">
                            <?php $newsFront = app\models\Article::find()->where(['category_id' => 1])->orderBy(['date_create' => SORT_DESC])->limit(6)->all(); ?>
                            <?php foreach ($newsFront as $newFront): ?>
                                <li>
                                    <div class="news-block-wrap">
                                        <div class="border-news-wrap">
                                            <div class="news-block">
                                                <div class="news-block-over">
                                                    <?php
                                                    $outputImg = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $newFront->body, $matches);
                                                    $first_img = $matches [1] [0];
                                                    ?>
                                                    <img class="img-responsive" src="<?= $first_img ?>" alt="" style="height: auto;;">
                                                    <div class="text-news"><?= \yii\helpers\StringHelper::truncate(strip_tags($newFront->body), 300) ?></div>
                                                </div>
                                            </div>
                                            <div class="bl-readmoe"><?= \yii\helpers\Html::a('Подробнее', ['/article/view', 'id' => $newFront->id], ['class' => 'btn btn-success']) ?></div>
                                        </div>
                                    </div>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div></div>
                <div class="obrashen">
                    <?= $fronContent->text ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="block-wrap-pichina fl-w">
    <div class="container">
        <h2 class="zagl-centr">5 причин обратиться именно к нам</h2>
        <div class="five-prichin"><div class="prich1 pichina"></div><div class="prich2 pichina"></div><div class="prich3 pichina"></div><div class="prich4 pichina"></div><div class="prich5 pichina"></div></div>
    </div>
</div>
<div class="block-wrap-rabota fl-w"><img src="/images/bg_center-1.jpg" alt=""></div>
