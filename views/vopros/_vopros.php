<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>



<div class="panel-group-<?= $model->id ?>" id="accordion">
    <div class="panel panel-default green-panel">
        <div class="panel-heading"><div class="right"><i class="fa fa-clock-o" aria-hidden="true"></i> <?= Yii::$app->formatter->asDate($model->date_create) ?></div>
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $model->id ?>">
                <h4 class="panel-title">
                    <?= $model->vopros ?>
                </h4>
            </a>
        </div>
        <div id="collapse<?= $model->id ?>" class="panel-collapse collapse">
            <div class="panel-body">
                <?= $model->otvet ?>
            </div>
        </div>
    </div>
</div>