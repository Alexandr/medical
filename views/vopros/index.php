<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\ActiveForm;
use yii\bootstrap\Alert;

/* @var $this yii\web\View */
/* @var $searchModel app\models\VoprosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Вопросы';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
if ($ref == 1) {
    echo Alert::widget([
        'options' => [
            'class' => 'alert-info',
        ],
        'body' => '<h1>Ваш вопрос отправлен</h1>',
    ]);
}
?>
<div class="vopros-index container">
    <div class="row">
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-3 right-bar">
            <div class="scoreboard">
                <h3> <i class="fa fa-question-circle-o" aria-hidden="true"></i>  Задать вопрос:</h3>
            </div>
<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'telephone')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'date_create')->hiddenInput(['value' => date('U')])->label(FALSE) ?>

            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'vopros')->textarea(['rows' => 6]) ?>

            <div class="form-group">
            <?= Html::submitButton('Отправить', ['class' => 'btn btn-success']) ?>
            </div>

                <?php ActiveForm::end(); ?>

        </div>
        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-9 content">

            <h1><?= Html::encode($this->title) ?></h1>
<?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

<?=
ListView::widget([
    'dataProvider' => $dataProvider,
    'itemOptions' => ['class' => 'item'],
    'itemView' => '_vopros',
])
?>
        </div>
    </div>
</div>
