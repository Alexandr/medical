<?php

use yii\helpers\Html;
use app\models\Services;
use app\models\FrontContent;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;

/* @var $this yii\web\View */
/* @var $model app\models\Services */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => $model->category->name, 'url' => [yii\helpers\Url::to(['/category-services/view', 'id' => $model->category->id])]];
$this->params['breadcrumbs'][] = $this->title;
$fronContent = FrontContent::findOne(1);
$services = Services::find()->all();
$price = \app\models\ServicesPrice::find()->where(['services_id' => $model->id,]);
$dataProvider = new ActiveDataProvider([
    'query' => $price,
        ]);
?>
<div class="services-view">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-3 right-bar">
                <div class="scoreboard">
                    <h3 class="fa-clock-o">  График работы:</h3>
                    <time class="h6"><?= $fronContent->graphik ?></time>
                </div>
                <?= app\components\accordion\AccordionMenu::widget(['categor' => $model->category->id, 'servise' => $model->id]) ?>
            </div>
            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-9 content">

                <h1><?= Html::encode($this->title) ?></h1>
                <div class="preview"><i class="fa fa-eye" aria-hidden="true"></i> Просмотров <?= $model->count ?></div>
                <p></p>
                <div class="row grid">
                    <?php foreach ($model->doctors as $doctor): ?>
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 doctor grid-item">
                            <div class="doctor-wrap">
                                <div class="imges">
                                    <img class="img-responsive" src="/web/<?= $doctor->getImage()->getPath('600x600') ?>" alt="">
                                </div>
                                <h3 class="title-doc"><?= Html::a($doctor->name, ['/doctor/view', 'id' => $model->id]) ?></h3>
                                <div class="opis-doctor">
                                    <?php foreach ($doctor->services as $serviceDoc): ?>
                                        <p><?= $serviceDoc->name ?></p>
                                    <?php endforeach; ?>
                                    <?= Html::a('Подробнее', ['/doctor/view', 'id' => $doctor->id], ['class' => 'btn btn-success btn-block']) ?>
                                </div>

                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>

                <div class="row">
                    <table class="table table-bordered table-hover">

                        <tbody>
                            <?php $contPr=0; foreach ($model->servicesPrices as $servicesPrice): ?>
                            <?php $contPr++; ?>
                                <tr>
                                    <td><?= $contPr ?></td>
                                    <td><b><?= $servicesPrice->name ?></b></td>
                                    <td class="w-115"><?= $servicesPrice->price ?> руб.</td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>

                </div>

                <div class="content-servise">
                    <?= $model->body ?>
                </div>
            </div>
        </div>
    </div>

</div>
