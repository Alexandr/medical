<?php

use yii\helpers\Html;
use yii\data\ActiveDataProvider;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $model app\models\Category */

$this->title = $model->name;
$this->params['breadcrumbs'][] = $this->title;
$article = app\models\Article::find()->where(['category_id' => $model->id])->with('category');

$dataProvider = new ActiveDataProvider([
    'query' => $article,
    'pagination' => [
        'pageSize' => 20,
    ],
    'sort' => [
        'defaultOrder' => [
            'date_create' => SORT_DESC,
        ]
    ],
        ]);
?>
<div class="container">
    <div class="row">
        <h1 class="text-center"><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="row">
        <div class="content"><?= $model->description ?></div>
    </div>
    <div class="row">
        <?=
        ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_post',
            'summary' => '',
        ]);
        ?>
    </div>
</div>
