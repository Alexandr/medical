<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
?>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="post-article">
        <div class="row">
            <div class="col-xs-12 col-sm-3 col-md-2 col-lg-2">
                <div class="date-post"><p><i class="fa fa-calendar" aria-hidden="true"></i> <?= Yii::$app->formatter->asDate($model->date_create) ?></p><p></p></div>
                <div class="count-post"><p><i class="fa fa-eye" aria-hidden="true"></i> Просмотров - <?= $model->count ?></p></div>
            </div>
            <div class="col-xs-12 col-sm-9 col-md-10 col-lg-10">
                <h2 class="post-article"><?= Html::a(Html::encode($model->name), ['/article/view', 'id' => $model->id]) ?></h2>
                <?php
                $outputImg = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $model->body, $matches);
                if (isset($matches [1] [0])) {
                    $first_img = $matches [1] [0];
                } else {
                    $first_img = '';
                }
                ?>
                <a href="/<?= yii\helpers\Url::to(['article/view', 'id' => $model->id]) ?>">
                    <img src="<?= $first_img ?>" style="float: left; margin-right: 8px" width="200px" height="auto" alt="">
                </a><div class="row">
                    <?= \yii\helpers\StringHelper::truncate(strip_tags($model->body), 350, '..') ?>
                </div>

                <?= Html::a('Подробнее', ['/article/view', 'id' => $model->id], ['class' => 'btn btn-default fl-r-btn']) ?>
            </div>
        </div>
    </div>
</div>