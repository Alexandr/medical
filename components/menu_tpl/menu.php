<?php
use app\models\Sef;

$sefLink = Sef::find()->where(['link' => $category['link']]);
//debug($sefLink->count());
if ($sefLink->count() > 0) {
    $ulrLink = '/'.$sefLink->one()->link_sef;
} else {
    $ulrLink = $category['link'];
}
    if ($ulrLink == Yii::$app->request->url) {
        $classLi = 'active';
    } else {
        $classLi = '';
    }
?>
<?php if (!isset($category['childs'])): ?>
    <li class="<?= $classLi ?>"><a href="<?= $ulrLink ?>" <?= $category['attribute'] ?> ><?= $category['name'] ?></a></li>
<?php else: ?>
    <li class="dropdown <?= $classLi ?>"><a href="<?= $ulrLink ?>"  class="dropdown-toggle" data-hover="dropdown" data-delay="100" data-toggle="dropdown" aria-expanded="false"><?= $category['name'] ?> <b class="caret"></b></a>
        <ul  class="dropdown-menu">
            <?= $this->getMenuHtml($category['childs']) ?>
        </ul>
    </li>
<?php endif; ?>


