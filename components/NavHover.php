<?php

namespace app\components;

use yii\base\Widget;
use yii\helpers\Url;

/**
 * Description of NavHover
 *
 * @author art
 */
class NavHover extends Widget {

    public $menu;
    public $arrMenu;

    public function init() {
        parent::init();
    }

    public function run() {
        if ($this->menu == 'top') {
            $MenuTop = new \app\models\MenuTop();
        } else {
            $MenuTop = new \app\models\MenuFooter();
        }

        $arrMenu = $MenuTop->getTree('tree');
        $templ = '<ul class="nav navbar-nav">';
        $templ .= $this->getMenuHtml($arrMenu);
        $templ .= '<ul>';
        return $templ;
    }
    
     protected function getMenuHtml($treesMenu){
        $tree = '';
        foreach ($treesMenu as $treeMenu) {
            $tree .= $this->catToTemplate($treeMenu);
        }
        return $tree;
    }
    
    protected function catToTemplate($category){
        ob_start();
        include __DIR__ . '/menu_tpl/menu.php';
        return ob_get_clean();
    }

}
