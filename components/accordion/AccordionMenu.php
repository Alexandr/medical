<?php

namespace app\components\accordion;

use app\models\CategoryServices;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AccordionMenu
 *
 * @author alex
 */
class AccordionMenu extends \yii\base\Widget {

    //put your code here
    public $categor;
    public $servise;

    public function init() {
        parent::init();
    }

    public function run() {
        $modelCategoryServices = new CategoryServices();
        $modelsCategoryTree = $modelCategoryServices->getTree('');
        $template = '<div class="panel-group uslu-bar id="accordion">';
        foreach ($modelsCategoryTree as $key => $modelCategoryTree) {
            $classCat = '';
            if ($this->categor) {
                if ($this->categor == $key) {
                    $classCat='in';
                }
            }
            $template .= $this->catToTemplate($key, $modelCategoryTree, $classCat, $this->servise);
        }
        $template .= '</div>';
        return $template;
    }
    
    protected function catToTemplate($k, $categoryTeml, $classCat, $idServ){
        ob_start();
        include __DIR__ . '/tpl/_accord.php';
        return ob_get_clean();
    }

}
