<?php

use app\models\Services;
use yii\helpers\Html;

$modelsServ = Services::find()->orderBy(['name' =>SORT_ASC])->where(['category_id' => $k])->all();
?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $k ?>">
                <?= $categoryTeml ?>
            </a>
        </h4>
    </div>
    <div id="collapse<?= $k ?>" class="panel-collapse collapse <?= $classCat ?>">
        <?= Html::a('<h4 class="green">Прайс '.mb_strtolower($categoryTeml).'</h4>', ['/category-services/price', 'id' => $k]) ?>
        <div class="panel-body">
            
            <ul class="nav nav-pills nav-stacked green-link">
                <?php foreach ($modelsServ as $modelServ): ?>
                <?php 
                $idDerv='';
                if ($idServ) {
                    if ($modelServ->id == $idServ) {
                       $idDerv='active'; 
                    }
                }
                ?>
                <li class="<?= $idDerv ?>"><?= Html::a($modelServ->name, ['/services/view', 'id' => $modelServ->id]) ?></li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
</div>