<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;

class SiteController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    
    public function actionSitemap()
    {
        $this->layout ='xml';
        return $this->render('sitemap');
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
                    'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact() {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
                    'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout() {
        return $this->render('about');
    }

    public function beforeAction($action) {

        $model = new \app\models\Search();
        if ($model->load(Yii::$app->request->post())&& $model->search) {
            $q = $model->search;
            return $this->redirect(Yii::$app->urlManager->createUrl(['/site/search', 'q' => $q]));
        }
        return TRUE;
    }

    public function actionSearch() {
        $q= Yii::$app->getRequest()->getQueryParam('q');
        $model = \app\models\Article::find()->where(['like', 'body', $q]);
        $modelServices = \app\models\Services::find()->orWhere(['like', 'name', $q])->orWhere(['like', 'body', $q]);
        $modelCategoryServices = \app\models\CategoryServices::find()->orWhere(['like', 'name', $q])->orWhere(['like', 'description', $q]);


        return $this->render('search', [
                    'q' => $q,
                    'model' => $model,
                    'modelServices' => $modelServices,
                    'modelCategoryServices' => $modelCategoryServices,
        ]);
    }
    
    public function actionPrice() {
        $this->layout = 'none';
        return $this->render('price');
    }

}
