<?php

namespace app\controllers;

use Yii;
use app\models\Article;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticleController extends Controller
{
    
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $model->count ++;
        $model->save();
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    
    protected function findModel($id)
    {
        if (($model = Article::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function beforeAction($action) {

        $model = new \app\models\Search();
        if ($model->load(Yii::$app->request->post())&& $model->search) {
            $q = $model->search;
            return $this->redirect(Yii::$app->urlManager->createUrl(['/site/search', 'q' => $q]));
        }
        return TRUE;
    }
    
}
