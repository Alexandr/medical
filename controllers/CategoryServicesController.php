<?php

namespace app\controllers;

use Yii;
use app\models\CategoryServices;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * CategoryServicesController implements the CRUD actions for CategoryServices model.
 */
class CategoryServicesController extends Controller
{

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    public function actionPrice($id)
    {
        return $this->render('price', [
            'model' => $this->findModel($id),
        ]);
    }

    protected function findModel($id)
    {
        if (($model = CategoryServices::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Такой записи не найдено');
        }
    }
    
    public function beforeAction($action) {

        $model = new \app\models\Search();
        if ($model->load(Yii::$app->request->post())&& $model->search) {
            $q = $model->search;
            return $this->redirect(Yii::$app->urlManager->createUrl(['/site/search', 'q' => $q]));
        }
        return TRUE;
    }
}
