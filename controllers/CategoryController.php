<?php

namespace app\controllers;

use app\models\Category;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use Yii;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends Controller
{


    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    protected function findModel($id)
    {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Такой записи не найдено.');
        }
    }
    
    public function beforeAction($action) {

        $model = new \app\models\Search();
        if ($model->load(Yii::$app->request->post())&& $model->search) {
            $q = $model->search;
            return $this->redirect(Yii::$app->urlManager->createUrl(['/site/search', 'q' => $q]));
        }
        return TRUE;
    }
}
