<?php

namespace app\controllers;

use Yii;
use app\models\Services;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * ServicesController implements the CRUD actions for Services model.
 */
class ServicesController extends Controller
{


    /**
     * Displays a single Services model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $service = $this->findModel($id);
        $service->count ++;
        $service->save();
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    
    protected function findModel($id)
    {
        if (($model = Services::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Такой записи не существует..');
        }
    }
    
    public function beforeAction($action) {

        $model = new \app\models\Search();
        if ($model->load(Yii::$app->request->post())&& $model->search) {
            $q = $model->search;
            return $this->redirect(Yii::$app->urlManager->createUrl(['/site/search', 'q' => $q]));
        }
        return TRUE;
    }
}
