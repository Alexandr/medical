<?php

namespace app\controllers;

use app\models\Comment;
use Yii;

class CommentController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $model = new Comment();
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/comment']);
        }
        return $this->render('index',[
            'model' => $model,
        ]);
    }

}
