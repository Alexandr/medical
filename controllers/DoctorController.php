<?php

namespace app\controllers;

use Yii;
use app\models\Doctor;
use app\models\DoctorSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Vopros;
use yii\bootstrap\Alert;

/**
 * DoctorController implements the CRUD actions for Doctor model.
 */
class DoctorController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Doctor models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DoctorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Doctor model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $modelVopros = new Vopros();
        
        if ($modelVopros->load(Yii::$app->request->post()) && $modelVopros->save()) {
            echo Alert::widget([
                'options' => [
                    'class' => 'alert-info',
                ],
                'body' => '<h1>Ваш вопрос отправлен</h1>',
            ]);
        }
        
        return $this->render('view', [
            'modelVopros' => $modelVopros,
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the Doctor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Doctor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Doctor::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function beforeAction($action) {

        $model = new \app\models\Search();
        if ($model->load(Yii::$app->request->post())&& $model->search) {
            $q = $model->search;
            return $this->redirect(Yii::$app->urlManager->createUrl(['/site/search', 'q' => $q]));
        }
        return TRUE;
    }
}
