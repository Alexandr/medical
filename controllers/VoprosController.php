<?php

namespace app\controllers;

use Yii;
use app\models\Vopros;
use app\models\VoprosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\bootstrap\Alert;

/**
 * VoprosController implements the CRUD actions for Vopros model.
 */
class VoprosController extends Controller {

    /**
     * Lists all Vopros models.
     * @return mixed
     */
    public function actionIndex() {
        $query = Vopros::find()->where(['not', ['otvet' => NULL]]);
        $model = new Vopros();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'date_create' => SORT_DESC,
                ]
            ],
        ]);


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/vopros/send']);
        }

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
                    'model' => $model,
                    'ref' => 0,
        ]);
    }

    public function actionSend() {
        $query = Vopros::find()->where(['not', ['otvet' => NULL]]);
        $model = new Vopros();
        $ref = 1;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/vopros/send']);
        }

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
                    'model' => $model,
                    'ref' => $ref,
        ]);
    }

    public function beforeAction($action) {

        $model = new \app\models\Search();
        if ($model->load(Yii::$app->request->post()) && $model->search) {
            $q = $model->search;
            return $this->redirect(Yii::$app->urlManager->createUrl(['/site/search', 'q' => $q]));
        }
        return TRUE;
    }

}
