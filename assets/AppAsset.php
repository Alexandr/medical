<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/justifiedGallery.css',
        'css/colorbox.css',
        'css/style.css',
    ];
    public $js = [
        'js/bootstrap.min.js',
        'js/bootstrap-hover-dropdown.min.js',
        'js/jquery.colorbox-min.js',
        'js/jquery.justifiedGallery.min.js',
        'js/jquery.flexslider-min.js',
        'js/masonry.pkgd.min.js',
        'js/main.js',
        'web/js/map-adr.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        
    ];
}
