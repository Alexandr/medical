<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "video".
 *
 * @property integer $id
 * @property integer $count
 * @property string $name
 * @property integer $date_create
 * @property string $video
 * @property string $alias
 */
class Video extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'video';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['count', 'name', 'date_create', 'video'], 'required'],
            [['count', 'date_create'], 'integer'],
            [['name', 'video', 'alias'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'count' => 'Count',
            'name' => 'Name',
            'date_create' => 'Date Create',
            'video' => 'Video',
            'alias' => 'Alias',
        ];
    }
}
