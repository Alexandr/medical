<?php

namespace app\models;

use Yii;
use app\models\Category;

/**
 * This is the model class for table "article".
 *
 * @property integer $id
 * @property string $name
 * @property integer $category_id
 * @property integer $date_create
 * @property string $body
 * @property integer $frontpage
 * @property integer $count
 * @property string $alias
 * @property integer $user_id
 *
 * @property Category $category
 * @property User $user
 */
class Article extends \yii\db\ActiveRecord {


    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'article';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name', 'category_id', 'date_create', 'count', 'user_id'], 'required'],
            [['category_id', 'date_create', 'frontpage', 'count', 'user_id'], 'integer'],
            [['name', 'alias'], 'string', 'max' => 255],
            [['body'], 'string'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => '№',
            'name' => 'Название',
            'category_id' => 'Категория',
            'date_create' => 'Дата создания',
            'body' => 'Текст',
            'frontpage' => 'На главной',
            'count' => 'Просмотров',
            'alias' => 'PATH',
            'user_id' => 'Пользователь',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory() {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    

}
