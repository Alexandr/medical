<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "services_price".
 *
 * @property integer $id
 * @property integer $services_id
 * @property string $name
 * @property integer $full_price
 * @property integer $only_service_prices
 *
 * @property Services $services
 */
class ServicesPrice extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'services_price';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['services_id', 'price'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['services_id'], 'exist', 'skipOnError' => true, 'targetClass' => Services::className(), 'targetAttribute' => ['services_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '№',
            'services_id' => 'Сервис',
            'name' => 'Название',
            'price' => 'Цена',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServices()
    {
        return $this->hasOne(Services::className(), ['id' => 'services_id']);
    }
}
