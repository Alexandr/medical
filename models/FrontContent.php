<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "front_content".
 *
 * @property integer $id
 * @property string $graphik
 * @property string $text
 */
class FrontContent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'front_content';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['graphik', 'text'], 'required'],
            [['graphik', 'text'], 'string'],
            [['telephone'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'graphik' => 'График работы',
            'text' => 'Текст',
            'telephone' => 'Телефон',
        ];
    }
}
