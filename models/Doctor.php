<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "doctor".
 *
 * @property integer $id
 * @property string $name
 * @property string $body
 * @property integer $date_create
 * @property integer $count
 * @property string $alias
 * @property integer $weight
 * @property integer $user_id
 *
 * @property Address $address
 * @property Services $services
 * @property User $user
 * @property DoctorSpecialization[] $doctorSpecializations
 */
class Doctor extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public $imageFile;
    public $services_array;

    public function behaviors() {
        return [
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ]
        ];
    }

    public static function tableName() {
        return 'doctor';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name', 'date_create', 'count', 'alias', 'weight', 'user_id'], 'required'],
            [['body'], 'string'],
            [['services_array'], 'safe'],
            [['imageFile'], 'file', 'extensions' => 'png, jpg'],
            [['date_create', 'count', 'weight', 'user_id'], 'integer'],
            [['name', 'alias'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => '№',
            'name' => 'Имя',
            'imageFile' => 'Изображение',
            'body' => 'Описание',
            'date_create' => 'Дата создания',
            'count' => 'Просмотры',
            'alias' => 'Path',
            'services_array' => 'Услуги',
            'weight' => 'Вес',
            'user_id' => 'Пользователь',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress() {
        return $this->hasOne(Address::className(), ['id' => 'address_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    /* public function getDoctorService()
      {
      return $this->hasMany(DoctorService::className(), ['doctor_id' => 'id']);
      } */

    public function getServices() {
        return $this->hasMany(Services::className(), ['id' => 'services_id'])->viaTable('doctor_service', ['doctor_id' => 'id']);
    }

    public function getServicesId() {
        return $this->services->id;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function upload() {
        if ($this->validate()) {
            $path = 'upload/images/' . $this->imageFile->baseName . '.' . $this->imageFile->extension;
            $this->imageFile->saveAs($path);
            $this->attachImage($path);
            unlink($path);
            return true;
        } else {
            return false;
        }
    }

}
