<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vopros".
 *
 * @property integer $id
 * @property integer $doctor_id
 * @property string $name
 * @property string $telephone
 * @property string $date_create
 * @property string $email
 * @property string $vopros
 * @property string $otvet
 *
 * @property Doctor $doctor
 */
class Vopros extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vopros';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['doctor_id'], 'integer'],
            [['name', 'telephone', 'date_create', 'email', 'vopros'], 'required'],
            [['vopros', 'otvet'], 'string'],
            [['name', 'telephone', 'date_create', 'email'], 'string', 'max' => 50],
            [['doctor_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '№',
            'doctor_id' => 'Врач',
            'name' => 'Имя',
            'telephone' => 'Телефон',
            'date_create' => 'Дата',
            'email' => 'Email',
            'vopros' => 'Вопрос',
            'otvet' => 'Ответ',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDoctor()
    {
        return $this->hasOne(Doctor::className(), ['id' => 'doctor_id']);
    }
}
