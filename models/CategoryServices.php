<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "category_services".
 *
 * @property integer $id
 * @property string $name
 * @property integer $weight
 * @property string $description
 * @property string $alias
 *
 * @property Services[] $services
 */
class CategoryServices extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category_services';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'weight', 'parent_id'], 'required'],
            [['weight'], 'integer'],
            [['parent_id'], 'integer'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['alias'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'parent_id' => 'Родитель',
            'weight' => 'Weight',
            'description' => 'Description',
            'alias' => 'Alias',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServices()
    {
        return $this->hasMany(Services::className(), ['category_id' => 'id']);
    }
    
    protected function Tree($cattrees, $tab = '', $tabs = '-') {
        $trees = [];
        $treesChild =[];
        foreach ($cattrees as $treecat) {
            $value = ArrayHelper::getValue($treecat, 'name');
            ArrayHelper::setValue($treecat, 'name', $tab.$value);
            if (!isset($treecat['childs'])) {
                $trees[] = $treecat;
            } 
            else {
                $trees[] = $treecat;
                foreach ($this->Tree($treecat['childs'],$tabs) as $t)
                    $trees[] = $t;
            }
        }
        return $trees;
    }

    public function getTree($tab = '-') {
        $tree = [];
        $catstree = CategoryServices::find()->indexBy('id')->asArray()->all();
        foreach ($catstree as $id => &$node) {
            if (!$node['parent_id'])
                $tree[$id] = &$node;
            else
                $catstree[$node['parent_id']]['childs'][$node['id']] = &$node;
        }
        ArrayHelper::multisort($tree, 'weight'); // Сортируем массив по полю weight. Если это необходимо
        $treeOne = ArrayHelper::map($this->Tree($tree, '', $tab), 'id', 'name'); //Вызываем функцию Tree и преобразуем его для select

        return $treeOne;
    }
}
