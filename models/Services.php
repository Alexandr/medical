<?php

namespace app\models;

use Yii;
use app\models\CategoryServices;
use app\models\Doctor;
use app\models\User;
use app\models\ServicesPrice;


/**
 * This is the model class for table "services".
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $name
 * @property string $body
 * @property integer $date_create
 * @property string $alias
 * @property integer $count
 * @property integer $user_id
 *
 * @property Doctor[] $doctors
 * @property Category $category
 * @property User $user
 * @property ServicesPrice[] $servicesPrices
 */
class Services extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'services';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'name', 'date_create', 'user_id'], 'required'],
            [['category_id', 'date_create', 'count', 'user_id'], 'integer'],
            [['alias'], 'safe'],
            [['body'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['alias'], 'string', 'max' => 50],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => CategoryServices::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '№',
            'category_id' => 'Категория услуг',
            'name' => 'Название',
            'body' => 'Описание',
            'date_create' => 'Дата создания',
            'alias' => 'PATH',
            'count' => 'Просмотры',
            'user_id' => 'Пользователь',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    
    public function getDoctorService()
    {
        return $this->hasMany(DoctorService::className(), ['services_id' => 'id']);
    }
    
    public function getDoctors()
    {
        return $this->hasMany(Doctor::className(), ['id' => 'doctor_id'])->via('doctorService');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(CategoryServices::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServicesPrices()
    {
        return $this->hasMany(ServicesPrice::className(), ['services_id' => 'id']);
    }
}
