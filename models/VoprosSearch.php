<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Vopros;

/**
 * VoprosSearch represents the model behind the search form about `app\models\Vopros`.
 */
class VoprosSearch extends Vopros {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'doctor_id'], 'integer'],
            [['name', 'telephone', 'date_create', 'email', 'vopros', 'otvet'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Vopros::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'date_create' => SORT_DESC,
                ]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'doctor_id' => $this->doctor_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
                ->andFilterWhere(['like', 'telephone', $this->telephone])
                ->andFilterWhere(['like', 'date_create', $this->date_create])
                ->andFilterWhere(['like', 'email', $this->email])
                ->andFilterWhere(['like', 'vopros', $this->vopros])
                ->andFilterWhere(['like', 'otvet', $this->otvet]);

        return $dataProvider;
    }

}
