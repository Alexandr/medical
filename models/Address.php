<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "address".
 *
 * @property integer $id
 * @property string $telephon
 * @property string $address
 *
 * @property Doctor[] $doctors
 */
class Address extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'address';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['address'], 'required'],
            [['telephon'], 'string', 'max' => 50],
            [['address'], 'string', 'max' => 255],
            [['map'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '№',
            'telephon' => 'Телофон',
            'address' => 'Адрес',
            'map' => 'Yandex MAP',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDoctors()
    {
        return $this->hasMany(Doctor::className(), ['address_id' => 'id']);
    }
}
