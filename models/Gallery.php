<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "gallery".
 *
 * @property integer $id
 * @property string $name
 * @property integer $date_create
 * @property string $description
 * @property string $alias
 */
class Gallery extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $images;
    
    
    public static function tableName()
    {
        return 'gallery';
    }
    
    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ]
        ];
    }
    

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_create'], 'required'],
            [['date_create'], 'integer'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 50],
            [['alias'], 'string', 'max' => 255],
            [['images'], 'file', 'extensions' => 'png, jpg', 'maxFiles' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'date_create' => 'Дата создания',
            'description' => 'Описание',
            'alias' => 'Alias',
            'images' => 'Изображения',
        ];
    }
    
   public function uploadGallery()
    {
        if ($this->validate()) {
            
            foreach ($this->images as $image) {
                $path = 'upload/images/' . $image->baseName . '.' . $image->extension;
                $image->saveAs($path);
                $this->attachImage($path);
                unlink($path);
            }

            return true;
        } else {
            return false;
        }
    }
}
