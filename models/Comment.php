<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "comment".
 *
 * @property integer $id
 * @property string $name
 * @property string $telephone
 * @property string $date_create
 * @property string $email
 * @property string $title
 * @property string $comment
 */
class Comment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'date_create', 'title', 'comment'], 'required'],
            [['comment'], 'string'],
            [['name', 'telephone', 'date_create', 'email', 'title'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'telephone' => 'Телефон',
            'date_create' => 'Дата создания',
            'email' => 'Email',
            'title' => 'Заголовок',
            'comment' => 'Отзыв',
        ];
    }
}
