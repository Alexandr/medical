<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "menu_footer".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $name
 * @property string $link
 * @property integer $weight
 */
class MenuFooter extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu_footer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'name', 'link', 'weight'], 'required'],
            [['parent_id', 'weight'], 'integer'],
            [['attribute'], 'string'],
            [['name'], 'string', 'max' => 50],
            [['link'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '№',
            'parent_id' => 'Родитель',
            'name' => 'Название',
            'link' => 'Ссылка',
            'attribute' => 'Атрибут',
            'weight' => 'Вес',
        ];
    }
    
    protected function Tree($cattrees, $tab = '') {
        $trees = [];
        $treesChild =[];
        foreach ($cattrees as $treecat) {
            $value = ArrayHelper::getValue($treecat, 'name');
            ArrayHelper::setValue($treecat, 'name', $tab.$value);
            if (!isset($treecat['childs'])) {
                $trees[] = $treecat;
            } 
            else {
                $trees[] = $treecat;
                foreach ($this->Tree($treecat['childs'],'-') as $t)
                    $trees[] = $t;
            }
        }
        return $trees;
    }
    


    public function getTree($treeMap) {
        $tree = [];
        $catstree = MenuFooter::find()->indexBy('id')->orderBy('weight')->asArray()->all();
        foreach ($catstree as $id => &$node) {
            if (!$node['parent_id'])
                $tree[$id] = &$node;
            else
                $catstree[$node['parent_id']]['childs'][$node['id']] = &$node;
        }

        if ($treeMap == 'map') {
            $treeOne = ArrayHelper::map($this->Tree($tree), 'id', 'name'); //Вызываем функцию Tree и преобразуем его для select
        } else {
            $treeOne = $tree;
        }
        
        return $treeOne;
    }
}
