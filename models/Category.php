<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property string $name
 * @property integer $parent_id
 * @property integer $weight
 * @property string $description
 * @property string $alias
 *
 * @property Article[] $articles
 * @property Diagnostics[] $diagnostics
 * @property Doctor[] $doctors
 * @property Services[] $services
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'parent_id', 'weight'], 'required'],
            [['parent_id', 'weight'], 'integer'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['alias'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '№',
            'name' => 'Название',
            'parent_id' => 'Родитель',
            'weight' => 'Вес',
            'description' => 'Описание',
            'alias' => 'PATH',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticles()
    {
        return $this->hasMany(Article::className(), ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiagnostics()
    {
        return $this->hasMany(Diagnostics::className(), ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDoctors()
    {
        return $this->hasMany(Doctor::className(), ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServices()
    {
        return $this->hasMany(Services::className(), ['category_id' => 'id']);
    }
    
    public function getParent()
    {
        return $this->hasOne(Category::className(), ['id' => 'parent_id']);
    }
    public function getParents()
    {
        return $this->hasMany(Category::className(), ['parent_id' => 'id']);
    }
    protected function Tree($cattrees, $tab = '') {
        $trees = [];
        $treesChild =[];
        foreach ($cattrees as $treecat) {
            $value = ArrayHelper::getValue($treecat, 'name');
            ArrayHelper::setValue($treecat, 'name', $tab.$value);
            if (!isset($treecat['childs'])) {
                $trees[] = $treecat;
            } 
            else {
                $trees[] = $treecat;
                foreach ($this->Tree($treecat['childs'],'-') as $t)
                    $trees[] = $t;
            }
        }
        return $trees;
    }

    public function getTree() {
        $tree = [];
        $catstree = Category::find()->indexBy('id')->asArray()->all();
        foreach ($catstree as $id => &$node) {
            if (!$node['parent_id'])
                $tree[$id] = &$node;
            else
                $catstree[$node['parent_id']]['childs'][$node['id']] = &$node;
        }
        ArrayHelper::multisort($tree, 'weight'); // Сортируем массив по полю weight. Если это необходимо
        $treeOne = ArrayHelper::map($this->Tree($tree), 'id', 'name'); //Вызываем функцию Tree и преобразуем его для select

        return $treeOne;
    }
    
}
