<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Doctor;

/**
 * DoctorSearch represents the model behind the search form about `app\models\Doctor`.
 */
class DoctorSearch extends Doctor {

    /**
     * 
     *
     * @inheritdoc
     */
    /* вычисляемый атрибут */

    public $servicesId;

    public function rules() {
        return [
            [['id', 'date_create', 'count', 'weight', 'user_id'], 'integer'],
            [['name', 'body', 'alias'], 'safe'],
            [['servicesId'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Doctor::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        //$this->addCondition($query, 'id');


        // grid filtering conditions
       $query->andFilterWhere([
            'doctor.id' => $this->id,
            'doctor.date_create' => $this->date_create,
            'doctor.count' => $this->count,
            'doctor.weight' => $this->weight,
            'doctor.user_id' => $this->user_id,
        ]);
        if ($this->servicesId) {
            $query->joinWith('services')->andFilterWhere(['services.id' => $this->servicesId]);
        }


        $query->andFilterWhere(['like', 'doctor.name', $this->name])
                ->andFilterWhere(['like', 'doctor.body', $this->body])
                ->andFilterWhere(['like', 'doctor.alias', $this->alias]);

        return $dataProvider;
    }

}
